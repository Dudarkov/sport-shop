<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html" charset=UTF-8">
<title>Регистрация</title>
</head>

<body>
<c:import url="header.jsp"/>
<div id="page-wrap">
<div class="col-md-2">
    <form:form id="formRegister" modelAttribute="user" action="register" method="post">
		<div class="form-group">
            <label for="username">Login</label>
            <p class="bg-danger"><form:errors path="username"/></p>
            <form:input path="username" class="form-control" id="username" name="username" placeholder="Login"/>
		</div>
		<div class="form-group">
			<label for="password">Password</label>
            <p class="bg-danger"><form:errors path="password"/></p>
			<form:password path="password" class="form-control" id="password" name="password" placeholder="Password"/>
		</div>
		<div class="form-group">
			<label for="repeat_password">Repeat Password</label>
            <p class="bg-danger"><form:errors path="repeatPassword"/></p>
			<form:password path="repeatPassword" class="form-control" id="repeat_password" name="repeat_password" placeholder="Password"/>
		</div>
		<div class="form-group">
			<label for="phone">Phone</label>
            <p class="bg-danger"><form:errors path="phone"/></p>
			<form:input path="phone" class="form-control" id="phone" name="phone" placeholder="+375-xx-xxx-xx-xx"/>
		</div>
		<div class="form-group">
			<label for="name">Name</label>
            <p class="bg-danger"><form:errors path="name"/></p>
			<form:input path="name" class="form-control" id="name" name="name" placeholder="Name"/>
		</div>
		<div class="form-group">
			<label for="surname">Surname</label>
            <p class="bg-danger"><form:errors path="surname"/></p>
			<form:input path="surname" class="form-control" id="surname" name="surname" placeholder="Surname"/>
		</div>
		<button type="submit" class="btn btn-default">Register</button>
	</form:form>
</div>
</div>
	<c:import url="footer.jsp"/>
</body>
</html>