<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="keywords" content="sportequipment,sport,equipment">
    <meta name="description" content="Sportequipment shop">
    <link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet" type="text/css">
    <link href="resources/img/favicon.png" rel="shortcut icon" type="image/x-icon">
    <title>Главная страница</title>
</head>
<body>

<c:import url="header.jsp"/>

<!--<div id="index"><img src="resources/img/just.jpg"/>-->
<div id="page-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-6">



            </div>
            <div class="col-md-6">
                <div id="carousel" class="carousel slide" data-ride="carousel" data-interval="3000">
                    <!-- Указатели -->

                    <c:set var="active" value="active"/>
                    <ol id="my-carousel-indicators" class="carousel-indicators">
                        <c:forEach begin="0" end="${fn:length(news)-1}" step="1" varStatus="i">
                            <li data-target="#carousel" data-slide-to="${i.index}" class="${active}"></li>
                            <c:set var="active" value=""/>
                        </c:forEach>
                    </ol>

                    <c:set var="active" value=" active"/>


                    <!-- Контент слайда (slider wrap)-->
                    <div class="carousel-inner">
                        <c:forEach var="news" items="${news}">
                            <div class="item${active}">
                                <img src="${pageContext.request.contextPath}/download/news/<c:out value="${news.iconName}"/>"
                                     alt="...">
                                <div class="carousel-caption">
                                        ${news.title}
                                </div>
                            </div>
                            <c:set var="active" value=""/>
                        </c:forEach>
                    </div>


                    <!-- Элементы управления -->
                    <a class="left carousel-control" href="#carousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" style="color:blue"/>
                    </a>
                    <a class="right carousel-control" href="#carousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true" style="color:blue"/>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<c:import url="footer.jsp"/>

</body>
</html>