<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset=UTF-8">
    <title>Новости</title>
</head>

<body>
<c:import url="header.jsp"/>
<div id="page-wrap">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th width="500">Image</th>
                    <th width="300">Title</th>
                    <th>Text</th>
                    <th width="200">Date</th>

                </tr>
                </thead>
                <tbody>
                <c:forEach var="news" items="${news}">
                    <tr>
                        <td> <img src="${pageContext.request.contextPath}/download/news/<c:out value="${news.iconName}"/>" ></td>
                        <td><c:out value="${news.title}"/></td>
                        <td><c:out value="${news.text}"/></td>
                        <td><c:out value="${news.dateNews}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</div>
<c:import url="footer.jsp"/>
</body>
</html>