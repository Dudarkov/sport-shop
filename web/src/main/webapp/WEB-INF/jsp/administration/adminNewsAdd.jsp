<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Добавление новости</title>
    <link href="resources/css/bootstrap.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="adminHeader.jsp"/>
<div id="page-wrap">
    <div class="row center-block">

        <div class="col-md-6">
            <form:form action="add" modelAttribute="news" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="title">Title</label>
                    <p class="bg-danger"><form:errors path="title"/></p>
                    <form:input path="title" class="form-control" id="title" />
                </div>

                <div class="form-group">
                    <label for="text">Text</label>
                    <p class="bg-danger"><form:errors path="text"/></p>
                    <form:textarea  path="text" class="form-control" rows="5" id="text" />
                </div>

                <div class="form-group">
                    <label for="date">Date</label>
                    <p class="bg-danger"><form:errors path="dateNews"/></p>
                    <form:input type="date" id="date" path="dateNews"/>
                </div>


                <div>
                    <label for="image">Image</label>
                    <p class="bg-danger"><form:errors path="file"/></p>
                    <form:input path="file" id="image" name="data" type="file" accept="image/jpeg" />
                </div>

                <button type="submit" class="btn btn-default">Добавить</button>
            </form:form>
        </div>

    </div>
</div>
</body>
</html>