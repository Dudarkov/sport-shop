<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Добавление товара</title>
    <link href="resources/css/bootstrap.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
</head>
<body>
<c:import url="adminHeader.jsp"/>
<div id="page-wrap">
    <div class="row center-block">

        <div class="col-md-2">
            <jsp:useBean id="addGoodForm" class="gmail.alexdudarkov.sportshop.forms.AddGoodForm" scope="request"/>
            <jsp:setProperty name="addGoodForm" property="*" />
            <form:form action="add" modelAttribute="good" method="post" enctype="multipart/form-data">
                <img name="image_name" src="${pageContext.request.contextPath}/admin/goods/uploadImage">
                <div class="form-group">
                    <label for="model">Model</label>
                    <p class="bg-danger"><form:errors path="model"/></p>
                    <form:input path="model" class="form-control" id="model"/>
                </div>

                <div class="form-group">
                    <label for="availability">Availability</label>
                    <form:select path="availability" id="availability" name="availability">
                        <option>true</option>
                        <option>false</option>
                    </form:select>
                </div>

                <div class="form-group">
                    <label for="brand">Brand</label>
                    <p class="bg-danger"><form:errors path="brand"/></p>
                    <form:select id="brand" path="brand.id">
                        <option selected disabled>Выберите бренд</option>
                        <c:forEach var="brand" items="${brands}">
                            <option value="${brand.id}">${brand.name}</option>
                        </c:forEach></form:select>
                </div>

                <div class="form-group">
                    <label for="type">Type</label>
                    <p class="bg-danger"><form:errors path="type"/></p>
                    <form:select id="type" path="type.id">
                        <option selected disabled>Выберите тип</option>
                        <c:forEach var="type" items="${types}">
                            <option value="${type.id}">${type.name}</option>
                        </c:forEach></form:select>
                </div>

                <div class="form-group">
                    <label for="price">Price</label>
                    <p class="bg-danger"><form:errors path="price"/></p>
                    <form:input type="number" path="price" id="price" min="0" step="0.01"/>
                </div>

                <div>
                    <label for="image">Image</label>
                    <p class="bg-danger"><form:errors path="file"/></p>
                    <form:input path="file" id="image" name="data" type="file" accept="image/jpeg"/>
                </div>

                <button type="submit" class="btn btn-default">Добавить</button>
            </form:form>
        </div>

</div>
</div>
</body>
</html>