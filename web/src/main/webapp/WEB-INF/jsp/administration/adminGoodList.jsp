<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Товары</title>
</head>
<body>
<c:import url="adminHeader.jsp"/>
<form:form  method="post">
    <table class="bordered">
        <tr>
            <td>Image</td>
            <td>Type</td>
            <td>Brand</td>
            <td>Model</td>
            <td>In stock</td>
            <td>Price</td>
            <td></td>
        </tr>
        <c:forEach var="good" items="${goods}">
            <tr>
                <td><img src="${pageContext.request.contextPath}/download/goods/<c:out value="${good.iconPath}"/>"
                         alt="Smiley face"
                         height="100" width="100"></td>
                <td>${good.type.name}</td>
                <td>${good.brand.name}</td>
                <td>${good.model}</td>
                <td>${good.availability}</td>
                <td>${good.price}</td>
                <td><a href="basket/add/${good.id}">В корзину</a><br></td>
            </tr>
        </c:forEach>
    </table>
    <c:forEach begin="1" end="${maxPage}" step="1" varStatus="i">
        <a href="catalog?page=${i.index}" >${i.index}</a>
    </c:forEach>
    <div class="right-side">
        <div>
            Brand
            <br>
            <c:forEach var="brand" items="${brands}">
                <input type="checkbox" name="brand" value="${brand.name}"/>${brand.name}<br>
            </c:forEach>
        </div>
        <br>
        <div>
            Type
            <br>
            <c:forEach var="type" items="${types}">
                <input type="checkbox" name="type" value="${type.name}"/>${type.name}<br>
            </c:forEach>
        </div>


        <input type="submit" formaction="Catalog" class="button" value="Подобрать"><br>
        <input type="submit" formaction="MyServlet?action=add_to_cart" class="button" value="Заказать"><br>
        <h3>${message}</h3>
    </div>
</form:form>
</body>
</html>