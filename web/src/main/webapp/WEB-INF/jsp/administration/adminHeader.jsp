<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/img/favicon.png" rel="shortcut icon" type="image/x-icon">


<header>
    <a href="${pageContext.request.contextPath}/index" title="На главную" id="logo">Sport shop</a>
    <span class="contact"><a href="${pageContext.request.contextPath}/about">О нас</a></span>
    <input type="text" class="field" placeholder="Найти">
    <span class="right">
        <a href="${pageContext.request.contextPath}/logout">Выход</a>
	</span>
</header>

<nav>
    <ul>
        <li><a href="${pageContext.request.contextPath}/index">Главная</a></li>
        <li><a href="${pageContext.request.contextPath}/admin/goods">Товары</a></li>
        <li><a href="${pageContext.request.contextPath}/admin/news">Новости</a></li>
        <li><a href="${pageContext.request.contextPath}/admin/order/list">Заказы</a></li>

        <sec:authorize access="hasRole('ROLE_SUPER_ADMIN')">
            <li><a href="${pageContext.request.contextPath}/superadmin/user/list">Пользователи</a></li>
        </sec:authorize>
    </ul>
</nav>

