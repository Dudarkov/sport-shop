<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Изменить пароль</title>
</head>
<body>
<c:import url="adminHeader.jsp"/>
<form:form  action="password" method="post" modelAttribute="user">
    <table  class="table table-bordered">
        <tr>
            <td>Name</td>
            <td>Surname</td>
            <td>Phone</td>
            <td>Role</td>
            <td>Status</td>
            <td>Login</td>
        </tr>

            <tr>
                <td>${user.name}</td>
                <td>${user.surname}</td>
                <td>${user.phone}</td>
                <td>${user.role}</td>
                <td>${user.status}</td>
                <td>${user.login}</td>

            </tr>

    </table>

    <div class="form-group">
        <form:input path="id" hidden="true"/>
    </div>
    <div class="form-group">
        <label for="password">New password</label>
        <p class="bg-danger"><form:errors path="password"/></p>
        <form:password path="password" class="form-control" id="password" name="password" placeholder="Password"/>
    </div>
    <button type="submit" class="btn btn-default">Change</button>
</form:form>
</body>
</html>