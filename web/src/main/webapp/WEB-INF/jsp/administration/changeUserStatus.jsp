<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Изменить статус</title>
</head>
<body>
<c:import url="adminHeader.jsp"/>
<form:form  action="status" method="post" modelAttribute="user">
    <table  class="table table-bordered">
        <tr>
            <td>Name</td>
            <td>Surname</td>
            <td>Phone</td>
            <td>Role</td>
            <td>Status</td>
            <td>Login</td>
        </tr>

        <tr>
            <td>${user.name}</td>
            <td>${user.surname}</td>
            <td>${user.phone}</td>
            <td>${user.role}</td>
            <td>${user.status}</td>
            <td>${user.login}</td>

        </tr>

    </table>

    <div class="form-group">
        <form:input path="id" hidden="true"/>
    </div>
    <div class="form-group">
        <label for="status">New status</label>
        <p class="bg-danger"><form:errors path="status"/></p>
        <form:select path="status" class="form-control" id="title" required="true">
            <form:option disabled="true" value="NONE" label="Select Status"/>
            <form:options items="${statuses}"/>
        </form:select>
    </div>
    <button type="submit" class="btn btn-default">Change</button>
</form:form>
</body>
</html>