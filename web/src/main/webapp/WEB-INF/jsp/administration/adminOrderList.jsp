<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Заказы</title>
</head>
<body>
<c:import url="adminHeader.jsp"/>
<form:form  method="post">
    <table  class="table table-bordered">
        <tr>
            <td>Name</td>
            <td>Surname</td>
            <td>Phone</td>
            <td>Date</td>
            <td>Status</td>
            <td></td>
        </tr>
        <c:forEach var="order" items="${orders}">
            <tr>
                <td>${order.user.name}</td>
                <td>${order.user.surname}</td>
                <td>${order.user.phone}</td>
                <td>${order.date}</td>
                <td>${order.status}</td>
                <td><a href="${pageContext.request.contextPath}/admin/order/change/status?id=${order.id}">Изменить статус</a></td>
            </tr>
        </c:forEach>
    </table>
</form:form>
</body>
</html>