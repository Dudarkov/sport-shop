<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <title>Изменение новости</title>
</head>
<body>
<c:import url="adminHeader.jsp"/>
<div id="page-wrap">
    <div class="row center-block">

        <form:form action="status" modelAttribute="order" method="post">

            <div class="col-md-4">

                <table class="table">

                    <tr>
                        <td>Name</td>
                        <td>${order.user.name}</td>
                    </tr>
                    <tr>
                        <td>Surname</td>
                        <td>${order.user.surname}</td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td>${order.user.phone}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>${order.date}</td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td>${order.status}</td>
                    </tr>
                </table>
                <div class="form-group">
                    <div class="form-group">
                        <form:input path="id" hidden="true"/>
                    </div>
                    <label for="title">Status</label>
                    <form:select path="status" class="form-control" id="title" required="true">
                        <form:option disabled="true" value="NONE" label="Select Role"/>
                        <form:options items="${statuses}"/>
                    </form:select>
                </div>
                <br>

                <button type="submit" class="btn btn-default">Изменить статус</button>
            </div>



            <div class="col-md-8">
                <table class="table table-bordered">
                    <tr>
                        <th>Image</th>
                        <th>Model</th>
                        <th>Brand</th>
                        <th>Type</th>
                        <th>Price</th>
                        <th>Count</th>
                    </tr>
                    <c:forEach var="orderItem" items="${orderItems}">
                        <tr>
                            <td><img
                                    src="${pageContext.request.contextPath}/download/goods/<c:out value="${orderItem.good.iconPath}"/>"
                                    alt="Smiley face"
                                    height="100" width="100"></td>
                            <td>${orderItem.good.model}</td>
                            <td>${orderItem.good.brand.name}</td>
                            <td>${orderItem.good.type.name}</td>
                            <td>${orderItem.price}</td>
                            <td>${orderItem.count}</td>
                        </tr>
                    </c:forEach>
                </table>
                <h3>Total Price: ${order.totalPrice}</h3>
            </div>



        </form:form>

    </div>
</div>
</body>
</html>