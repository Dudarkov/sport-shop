<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Новости</title>
</head>
<body>
<c:import url="adminHeader.jsp"/>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>#</th>
                    <th>Title</th>
                    <th>Text</th>
                    <th>Date</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="news" items="${news}">
                    <tr>
                        <td> <img src="${pageContext.request.contextPath}/download/news/<c:out value="${news.iconName}"/>" alt="..."  height="100" width="100"></td>
                        <td><c:out value="${news.id}"/></td>
                        <td><c:out value="${news.title}"/></td>
                        <td><c:out value="${news.text}"/></td>
                        <td><c:out value="${news.dateNews}"/></td>
                        <td><a href="delete/${news.id}">Удалить</a> <br>
                        <a href="update?id=${news.id}">Обновить</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>
