<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Новости</title>
</head>
<body>
<c:import url="adminHeader.jsp"/>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <table class="table table-bordered">
                <thead>
                <tr>

                    <th>#</th>
                    <th>Login</th>
                    <th>Role</th>
                    <th>Status</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Phone</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="user" items="${users}">
                    <tr>

                        <td><c:out value="${user.id}"/></td>
                        <td><c:out value="${user.login}"/></td>
                        <td><c:out value="${user.role}"/></td>
                        <td><c:out value="${user.status}"/></td>
                        <td><c:out value="${user.name}"/></td>
                        <td><c:out value="${user.surname}"/></td>
                        <td><c:out value="${user.phone}"/></td>
                        <td><a href="change/role?id=${user.id}">Изменить роль</a> <br>
                            <a href="change/password?id=${user.id}">Изменить пароль</a><br>
                            <a href="change/status?id=${user.id}">Изменить статус</a> <br>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>
