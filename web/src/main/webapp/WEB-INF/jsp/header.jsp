<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/img/favicon.png" rel="shortcut icon" type="image/x-icon">


<header>
    <a href="${pageContext.request.contextPath}/index" title="На главную" id="logo">Sport shop</a>
    <span class="contact"><a href="${pageContext.request.contextPath}/about">О нас</a></span>
    <input type="text" class="field" placeholder="Найти">
    <span class="right">

		   		<span class="contact">
		   			<a href="${pageContext.request.contextPath}/register" title="Зарегистрироваться">Регистрация</a>
		   		</span>
                <sec:authorize access="!isAuthenticated()">
		   		    <span class="contact">
		    		    <a href="${pageContext.request.contextPath}/login" title="Войти">Вход</a>
		   		    </span>
                </sec:authorize>
		   		<sec:authorize access="isAuthenticated()">
					<span class="contact">
					    <a href="${pageContext.request.contextPath}/logout">Выход</a>
					</span>
                </sec:authorize>

	</span>
</header>

<nav>
    <ul>
        <li><a href="${pageContext.request.contextPath}/index">Главная</a></li>
        <li><a href="${pageContext.request.contextPath}/catalog?page=1">Каталог</a></li>
        <li><a href="${pageContext.request.contextPath}/comment">Контакты</a></li>
        <li><a href="${pageContext.request.contextPath}/basket/list">Корзина</a></li>
        <li><a href="${pageContext.request.contextPath}/user/profile">Профиль</a></li>
        <li><a href="${pageContext.request.contextPath}/order/list">Заказы</a></li>
        <li><a href="${pageContext.request.contextPath}/news/list">Новости</a></li>
        <sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_SUPER_ADMIN')">
            <li><a href="${pageContext.request.contextPath}/admin/adminMenu">Администрирование</a></li>
        </sec:authorize>
    </ul>
</nav>
	

