<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Контакты</title>
</head>
<body>
<c:import url="header.jsp"/>
<div id="page-wrap">
    <h2>Третья улица строителей, д.25, кв. 12</h2>
    <sec:authorize access="hasRole('ROLE_USER')">
    <div class="row center-block">

        <div class="col-md-6">

            <form:form method="post" action="comment/add" modelAttribute="comment">
                <div class="form-group">
                    <label for="email">Email</label>
                    <p class="bg-danger"><form:errors path="email"/></p>
                    <form:input type="email" path="email" name="Email"/>
                </div>

                <div class="form-group">
                    <label for="content">Text</label>
                    <p class="bg-danger"><form:errors path="content"/></p>
                    <form:textarea path="content" name="content" rows="5"/>
                </div>
                <button class="btn btn-default">Отправить</button>
            </form:form>
        </div>
    </div>
    </sec:authorize>
</div>

<c:import url="footer.jsp"/>
</body>
</html>