<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Заказы</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link href="<c:url value="${pageContext.request.contextPath}/resources/css/style.css"/>" rel="stylesheet"
          type="text/css">
    <link href="<c:url value="${pageContext.request.contextPath}/resources/img/favicon.png"/>" rel="shortcut icon"
          type="image/x-icon">

</head>
<body>
<c:import url="../header.jsp"/>
<div id="page-wrap">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="order" items="${orders}">
                    <tr>
                        <th><c:out value="${order.date}"/></th>
                        <td><c:out value="${order.status}"/></td>
                        <td><a href="${pageContext.request.contextPath}/order/items?id=${order.id}">Подробнее</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
<c:import url="../footer.jsp"/>

</body>
</html>
