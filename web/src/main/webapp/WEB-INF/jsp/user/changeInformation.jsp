<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Профиль</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <link href="<c:url value="${pageContext.request.contextPath}/resources/css/style.css"/>" rel="stylesheet" type="text/css">
    <link href=<c:url value="${pageContext.request.contextPath}/resources/img/favicon.png"/>"" rel="shortcut icon" type="image/x-icon">
</head>
<title>Сменить пароль</title>
</head>
<body>
<c:import url="../header.jsp"/>
<div id="page-wrap">
    <div class="col-md-2">
        <form:form id="formRegister" modelAttribute="userInformation" action="information" method="post">
            <div class="form-group">
                <label for="name">Name</label>
                <p class="bg-danger"><form:errors path="name"/></p>
                <form:input path="name" class="form-control" name="name" placeholder="Name"/>
            </div>
            <div class="form-group">
                <label for="surname">Surname</label>
                <p class="bg-danger"><form:errors path="surname"/></p>
                <form:input path="surname" class="form-control"  name="surname" placeholder="Surname"/>
            </div>
            <div class="form-group">
                <label for="phone">Phone</label>
                <p class="bg-danger"><form:errors path="phone"/></p>
                <form:input path="phone" class="form-control" name="phone" placeholder="Phone"/>
            </div>
            <button type="submit" class="btn btn-default">Change</button>
        </form:form>
    </div>
</div>
<c:import url="../footer.jsp"/>

</body>
</html>
