<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>

    <title>Добавить товар</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <link href="<c:url value="${pageContext.request.contextPath}/resources/css/style.css"/>" rel="stylesheet" type="text/css">
    <link href=<c:url value="${pageContext.request.contextPath}/resources/img/favicon.png"/>"" rel="shortcut icon" type="image/x-icon">

</head>
<body>
<c:import url="../header.jsp"/>
<div id="page-wrap">
    <div class="row">
    <div class="col-md-6">
        <form:form modelAttribute="basketItem" action="good/${basketItem.good.id}">
            <table class="table">

                <tr>
                    <td>Model</td>
                    <td>${basketItem.good.model}</td>
                </tr>
                <tr>
                    <td>Price</td>
                    <td>${basketItem.good.price}</td>
                </tr>
                <tr>
                    <td>Brand</td>
                    <td> ${basketItem.good.brand.name}</td>
                </tr>
                <tr>
                    <td>Type</td>
                    <td> ${basketItem.good.type.name}</td>
                </tr>
            </table>
            <label for="password">Count</label>
            <form:input type="number" path="count" min="1" name="password"/>
            <button type="submit" class="btn btn-default">В корзину</button>
        </form:form>

    </div>
        <div class="col-md-6">
            <td><img src="${pageContext.request.contextPath}/download/goods/<c:out value="${basketItem.good.iconPath}"/>" alt="Smiley face"
                     height="150" width="150"></td>
        </div>
    </div>
</div>
<c:import url="../footer.jsp"/>

</body>
</html>
