<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Заказы</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <link href="<c:url value="${pageContext.request.contextPath}/resources/css/style.css"/>" rel="stylesheet"
          type="text/css">
    <link href="<c:url value="${pageContext.request.contextPath}/resources/img/favicon.png"/>" rel="shortcut icon"
          type="image/x-icon">

</head>
<body>
<c:import url="../header.jsp"/>
<div id="page-wrap">
    <div class="row">
        <div class="col-md-8">
            <table class="table table-bordered">
                <tr>
                    <th>Image</th>
                    <th>Model</th>
                    <th>Brand</th>
                    <th>Type</th>
                    <th>Price</th>
                    <th>Count</th>
                </tr>
                <c:forEach var="orderItem" items="${orderItems}">
                    <tr>
                        <td><img
                                src="${pageContext.request.contextPath}/download/goods/<c:out value="${orderItem.good.iconPath}"/>"
                                alt="Smiley face"
                                height="100" width="100"></td>
                        <td>${orderItem.good.model}</td>
                        <td>${orderItem.good.brand.name}</td>
                        <td>${orderItem.good.type.name}</td>
                        <td>${orderItem.price}</td>
                        <td>${orderItem.count}</td>
                    </tr>
                </c:forEach>
            </table>
            <h3>Total Price: ${order.totalPrice}</h3>
        </div>
    </div>
</div>
<c:import url="../footer.jsp"/>

</body>
</html>