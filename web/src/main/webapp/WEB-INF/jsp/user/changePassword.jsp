<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Профиль</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <link href="<c:url value="${pageContext.request.contextPath}/resources/css/style.css"/>" rel="stylesheet" type="text/css">
    <link href=<c:url value="${pageContext.request.contextPath}/resources/img/favicon.png"/>"" rel="shortcut icon" type="image/x-icon">
</head>
    <title>Сменить пароль</title>
</head>
<body>
<c:import url="../header.jsp"/>
<div id="page-wrap">
<div class="col-md-2">
<form:form id="formRegister" modelAttribute="changePassword" action="password" method="post">
<div class="form-group">
    <label for="password">Password</label>
    <p class="bg-danger"><form:errors path="password"/></p>
    <form:password path="password" class="form-control" id="password" name="password" placeholder="Password"/>
</div>
<div class="form-group">
    <label for="new_password">New Password</label>
    <p class="bg-danger"><form:errors path="newPassword"/></p>
    <form:password path="newPassword" class="form-control" id="new_password" name="new_password" placeholder="Password"/>
</div>
<div class="form-group">
    <label for="repeat_new_password">Repeat New Password</label>
    <p class="bg-danger"><form:errors path="repeatNewPassword"/></p>
    <form:password path="repeatNewPassword" class="form-control" id="repeat_new_password" name="repeat_new_password" placeholder="Password"/>
</div>
    <button type="submit" class="btn btn-default">Change Password</button>
    </form:form>
</div>
</div>
<c:import url="../footer.jsp"/>

</body>
</html>
