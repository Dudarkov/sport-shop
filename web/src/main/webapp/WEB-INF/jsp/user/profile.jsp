<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Профиль</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <link href="<c:url value="${pageContext.request.contextPath}/resources/css/style.css"/>" rel="stylesheet" type="text/css">
    <link href=<c:url value="${pageContext.request.contextPath}/resources/img/favicon.png"/>"" rel="shortcut icon" type="image/x-icon">
</head>
<body>
<c:import url="../header.jsp"/>
<div id="page-wrap">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <table class="table">
                <tr>
                    <td>Name</td>
                    <td>${user.name}</td>
                </tr>
                <tr>
                    <td>Surname</td>
                    <td>${user.surname}</td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td> ${user.phone}</td>
                </tr>
                <tr>
                    <td>Login</td>
                    <td> ${user.login}</td>
                </tr>
            </table>
        </div>
        <div class="col-md-4"> <a href="change/password">Сменить пароль</a> <br>
        <a href="change/information">Сменить информацию</a> </div>
    </div>
</div>
</div>
<c:import url="../footer.jsp"/>
</body>
</html>
