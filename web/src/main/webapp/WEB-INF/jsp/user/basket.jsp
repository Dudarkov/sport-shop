<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Корзина</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <link href="<c:url value="${pageContext.request.contextPath}/resources/css/style.css"/>" rel="stylesheet"
          type="text/css">
    <link href="<c:url value="${pageContext.request.contextPath}/resources/img/favicon.png"/>" rel="shortcut icon"
          type="image/x-icon">

</head>
<body>
<c:import url="../header.jsp"/>
<div id="page-wrap">
    <h2>${message}</h2>
<c:if test="${!empty basket.basketItems}">
    <div class="row">
        <div class="col-md-6">
            <form:form modelAttribute="basket" action="${pageContext.request.contextPath}/order/add" method="post">
                <table class="table">
                    <tr>
                        <td>Image</td>
                        <td>Type</td>
                        <td>Brand</td>
                        <td>Model</td>
                        <td>Count</td>
                        <td>Price</td>
                        <td></td>
                    </tr>
                    <c:forEach var="basketItem" items="${basket.basketItems}">
                        <tr>
                            <td><img src="${pageContext.request.contextPath}/download/goods/<c:out value="${basketItem.good.iconPath}"/>" alt="Smiley face"
                                     height="42" width="42"></td>
                            <td>${basketItem.good.type.name}</td>
                            <td>${basketItem.good.brand.name}</td>
                            <td>${basketItem.good.model}</td>
                            <td>${basketItem.count}</td>
                            <td>${basketItem.price}</td>
                            <td><a href="delete?id=${basketItem.id}">Убрать</a><br></td>
                        </tr>
                    </c:forEach>
                </table>

                <button type="submit" class="btn btn-default">Заказать</button>
                <button type="submit" class="btn btn-default" formaction="basket/clear">Очистить корзину</button>
            </form:form>

        </div>
        <div class="col-md-6">
            <h2>Total price</h2><br>
            ${basket.totalPrice}
            <div>
    </div>
            </c:if>
</div>
<c:import url="../footer.jsp"/>


</body>
</html>
