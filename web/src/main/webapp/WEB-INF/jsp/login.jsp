<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="input" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Вход</title>
</head>
<body>
<c:import url="header.jsp"/>
<div id="page-wrap">
    <div class="col-md-2">

        <form action="login" method="post">
            <c:if test="${param['error']}">
            <p class="bg-danger">Username or password is not valid</p>
            </c:if>
            <div class="form-group">
                <label for="username">Login</label>
                <input type="text" class="form-control" id="username" required name="username" placeholder="Login"/>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Password"/>
            </div>
            <button type="submit" class="btn btn-default">Login</button>
        </form>
    </div>
</div>
<c:import url="footer.jsp"/>
</body>
</html>