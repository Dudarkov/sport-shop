package gmail.alexdudarkov.sportshop.controllers;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.CommentService;
import gmail.alexdudarkov.sportshop.service.model.CommentDTO;
import gmail.alexdudarkov.sportshop.validators.CommentValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CommentController {
    private final CommentService commentService;
private final CommentValidator commentValidator;
    @Autowired
    public CommentController(CommentService commentService, CommentValidator commentValidator) {
        this.commentService = commentService;
        this.commentValidator = commentValidator;
    }

    @RequestMapping(value = "/comment", method = RequestMethod.GET)
    public String contactsPage(@ModelAttribute(value = "comment") CommentDTO comment, ModelMap model) {

        return "contacts";
    }

    @RequestMapping(value = "/comment/add", method = RequestMethod.POST)
    public String saveComment(@ModelAttribute(value = "comment") CommentDTO comment, BindingResult result) throws DaoException {
        commentValidator.validate(comment, result);
        if (!result.hasErrors()) {
            commentService.saveComment(comment);
            return "redirect:/comment";
        } else {
            return "contacts";
        }
    }
}
