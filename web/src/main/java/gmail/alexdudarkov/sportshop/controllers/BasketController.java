package gmail.alexdudarkov.sportshop.controllers;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.BasketItemService;
import gmail.alexdudarkov.sportshop.service.BasketService;
import gmail.alexdudarkov.sportshop.service.GoodService;
import gmail.alexdudarkov.sportshop.service.exception.ServiceException;
import gmail.alexdudarkov.sportshop.service.model.AppUserPrincipal;
import gmail.alexdudarkov.sportshop.service.model.BasketDTO;
import gmail.alexdudarkov.sportshop.service.model.BasketItemDTO;
import gmail.alexdudarkov.sportshop.service.model.GoodDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller(value = "/basket")
public class BasketController {

    private final BasketItemService basketItemService;
    private final GoodService goodService;
    private final BasketService basketService;
    @Autowired
    private Environment environment;

    @Autowired
    public BasketController(BasketItemService basketItemService, GoodService goodService, BasketService basketService) {
        this.basketItemService = basketItemService;
        this.goodService = goodService;
        this.basketService = basketService;
    }

    @RequestMapping(value = "/basket/list", method = RequestMethod.GET)
    public String basketForm(ModelMap modelMap) throws DaoException {
        BasketDTO basket = basketService.getUserBasket();

        if (basket.getBasketItems().isEmpty()) {
            String message = environment.getProperty("message.basket.empty");
            modelMap.addAttribute("message", message);
        }

        modelMap.addAttribute("basket", basket);

        return "user/basket";
    }

    @RequestMapping(value = "/basket/delete", method = RequestMethod.GET)
    public String deleteBasketItem(@RequestParam("id") Long id, ModelMap modelMap) throws DaoException {
        basketService.deleteItemFromBasket(id);


        return "redirect:/basket/list";
    }


    @RequestMapping(value = "/basket/add/{id}", method = RequestMethod.GET)
    public String basketAddForm(@ModelAttribute(value = "basketItem") BasketItemDTO basketItemDTO, ModelMap modelMap, @PathVariable Long id) throws DaoException {
        basketItemDTO.setGood(goodService.getById(id));
        return "user/addBasketItem";
    }

    @RequestMapping(value = "/basket/add/good/{id}", method = RequestMethod.POST)
    public String addToBasket(@ModelAttribute(value = "basketItem") BasketItemDTO basketItemDTO, ModelMap modelMap, @PathVariable Long id) throws DaoException {
        AppUserPrincipal authentication = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        Long idUser = authentication.getUserId();
        GoodDTO goodDTO = goodService.getById(id);
        basketItemDTO.setGood(goodDTO);
        basketItemDTO.setPrice(goodDTO.getPrice());
        basketItemDTO.setBasketId(idUser);
        basketItemService.save(basketItemDTO);
        return "redirect:/catalog?page=1";
    }

    @RequestMapping(value = "/basket/clear", method = RequestMethod.POST)
    public String clearBasket(ModelMap modelMap) {
        AppUserPrincipal authentication = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        Long id = authentication.getUserId();
        basketService.clearBasket(id);
        return "user/basket";
    }

}
