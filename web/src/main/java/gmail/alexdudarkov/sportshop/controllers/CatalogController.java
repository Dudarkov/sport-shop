package gmail.alexdudarkov.sportshop.controllers;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.BrandGoodService;
import gmail.alexdudarkov.sportshop.service.GoodService;
import gmail.alexdudarkov.sportshop.service.TypeGoodService;
import gmail.alexdudarkov.sportshop.service.exception.ServiceException;
import gmail.alexdudarkov.sportshop.service.model.BrandGoodDTO;
import gmail.alexdudarkov.sportshop.service.model.GoodDTO;
import gmail.alexdudarkov.sportshop.service.model.TypeGoodDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class CatalogController {

    @Autowired
    private GoodService goodService;
    @Autowired
    private BrandGoodService brandGoodService;
    @Autowired
    private TypeGoodService typeGoodService;

    @RequestMapping(value = "/catalog", method = RequestMethod.GET)
    public String catalogPage( @RequestParam("page") Integer page, ModelMap model) throws ServiceException, DaoException {
        List<GoodDTO> goods=goodService.getSome(page,10);
        List<BrandGoodDTO> brands=brandGoodService.findAll();
        List<TypeGoodDTO> types=typeGoodService.findAll();
        Long maxPage=goodService.getCountOfPage(10);
        model.addAttribute("maxPage", maxPage);
        model.addAttribute("goods", goods);
        model.addAttribute("brands", brands);
        model.addAttribute("types", types);


        return "catalog";
    }

    @RequestMapping(value = "/catalog", method = RequestMethod.POST)
    public String basketPage(@RequestParam("item") Integer item, ModelMap model) throws ServiceException, DaoException {
       System.out.println(item);


        return "catalog";
    }
}
