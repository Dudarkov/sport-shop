package gmail.alexdudarkov.sportshop.controllers.admin;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.NewsService;
import gmail.alexdudarkov.sportshop.service.model.NewsDTO;
import gmail.alexdudarkov.sportshop.validators.NewsValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Controller
@RequestMapping(value = "admin/news")
public class AdminNewsController {

    private final NewsService newsService;
    private final NewsValidator newsValidator;

    @Autowired
    public AdminNewsController(NewsService newsService, NewsValidator newsValidator) {
        this.newsService = newsService;
        this.newsValidator = newsValidator;
    }


    @RequestMapping(method = RequestMethod.GET)
    public String newsMenuForm(ModelMap modelMap) {
        return "administration/adminNews";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/add")
    public String addNewsForm(@ModelAttribute(value = "news") NewsDTO news, ModelMap modelMap) {

        return "administration/adminNewsAdd";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add")
    public String addNews(@ModelAttribute(value = "news") NewsDTO news, BindingResult result, ModelMap modelMap) throws DaoException {
        newsValidator.validate(news, result);

        if (!result.hasErrors()) {
            newsService.save(news);
            return "redirect:add";
        } else {
            return "administration/adminNewsAdd";
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/update")
    public String updateNewsForm(@RequestParam("id") Long id, ModelMap modelMap) throws DaoException {

        NewsDTO news=newsService.findById(id);
        modelMap.addAttribute("news", news);
        return "administration/adminNewsUpdate";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public String updateNews(@ModelAttribute(value = "news") NewsDTO news, BindingResult result, ModelMap modelMap) throws DaoException, IOException {
        newsValidator.validate(news, result);

        if (!result.hasErrors()) {
            newsService.update(news);
            return "redirect:add";
        } else {
            return "administration/adminNewsUpdate";
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/list")
    public String listNewsForm( ModelMap modelMap) throws DaoException {
       modelMap.addAttribute("news", newsService.findAll());
        return "administration/adminNewsList";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/delete/{id}")
    public String deleteNews(ModelMap modelMap,@PathVariable Long id) throws DaoException {
       NewsDTO newsDTO= newsService.findById(id);
       if (newsDTO==null){
           return "forward:/admin/news/list";
       } else {
           newsService.delete(newsDTO);
           return "forward:/admin/news/list";
       }
    }


}
