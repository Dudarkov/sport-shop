package gmail.alexdudarkov.sportshop.controllers.admin;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.BrandGoodService;
import gmail.alexdudarkov.sportshop.service.GoodService;
import gmail.alexdudarkov.sportshop.service.TypeGoodService;
import gmail.alexdudarkov.sportshop.service.exception.ServiceException;
import gmail.alexdudarkov.sportshop.service.model.BrandGoodDTO;
import gmail.alexdudarkov.sportshop.service.model.GoodDTO;
import gmail.alexdudarkov.sportshop.service.model.TypeGoodDTO;
import gmail.alexdudarkov.sportshop.validators.GoodValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "admin/goods")
public class AdminGoodsController {

    private final BrandGoodService brandGoodService;
    private final TypeGoodService typeGoodService;
    private final GoodService goodService;
    private final GoodValidator goodValidator;
    @Autowired
    public AdminGoodsController(BrandGoodService brandGoodService, TypeGoodService typeGoodService, GoodService goodService, GoodValidator goodValidator) {
        this.brandGoodService = brandGoodService;
        this.typeGoodService = typeGoodService;
        this.goodService = goodService;
        this.goodValidator = goodValidator;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String goodsMenuForm(ModelMap modelMap) {
        return "administration/adminGoods";
    }

    @RequestMapping(value = "/brand/add", method = RequestMethod.GET)
    public String addBrandForm(@ModelAttribute("brand") BrandGoodDTO brandGoodDTO, ModelMap modelMap) {


        return "administration/adminGoodsBrandAdd";

    }

    @RequestMapping(value = "/brand/add", method = RequestMethod.POST)
    public String addBrand(@ModelAttribute("brand") BrandGoodDTO brandGoodDTO, ModelMap modelMap) {
        try {
            brandGoodService.save(brandGoodDTO);
        } catch (ServiceException e) {
            e.printStackTrace();
        } catch (DaoException e) {
            e.printStackTrace();
        }

        return "redirect:/admin/goods/brand/add";

    }

    @RequestMapping(value = "/type/add", method = RequestMethod.GET)
    public String addTypeForm(@ModelAttribute("type") TypeGoodDTO brandGoodDTO, ModelMap modelMap) {


        return "administration/adminGoodsTypeAdd";

    }

    @RequestMapping(value = "/type/add", method = RequestMethod.POST)
    public String addType(@ModelAttribute("type") TypeGoodDTO typeGoodDTO, ModelMap modelMap) {
        try {
            typeGoodService.save(typeGoodDTO);
        } catch (ServiceException e) {
            e.printStackTrace();
        } catch (DaoException e) {
            e.printStackTrace();
        }

        return "redirect:/admin/goods/type/add";

    }

    @RequestMapping(value = "/good/add", method = RequestMethod.GET)
    public String addGoodForm(@ModelAttribute("good") GoodDTO goodDTO, ModelMap modelMap){
        try {
            modelMap.addAttribute("brands", brandGoodService.findAll());
            modelMap.addAttribute("types", typeGoodService.findAll());
        } catch (ServiceException e) {
            e.printStackTrace();
        } catch (DaoException e) {
            e.printStackTrace();
        }

        return "administration/adminGoodsAdd";
    }

    @RequestMapping(value = "/good/add", method = RequestMethod.POST)
    public String addGood(@ModelAttribute("good") GoodDTO goodDTO, BindingResult result, ModelMap modelMap) {
        System.out.println(goodDTO);

        goodValidator.validate(goodDTO, result);
        if (!result.hasErrors()) {
            try {
                goodService.save(goodDTO);
            } catch (ServiceException e) {
                e.printStackTrace();
            } catch (DaoException e) {
                e.printStackTrace();
            }
            return "redirect:add";
        } else {
            try {
                modelMap.addAttribute("brands", brandGoodService.findAll());
                modelMap.addAttribute("types", typeGoodService.findAll());
            } catch (ServiceException e) {
                e.printStackTrace();
            } catch (DaoException e) {
                e.printStackTrace();
            }
            return "administration/adminGoodsAdd";
        }

    }
}