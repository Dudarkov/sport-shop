package gmail.alexdudarkov.sportshop.controllers;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.NewsService;
import gmail.alexdudarkov.sportshop.service.model.NewsDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.List;

@Controller
public class HelloController {

    private final NewsService newsService;

    public HelloController(NewsService newsService) {
        this.newsService = newsService;
    }

    @RequestMapping(value = {"/index","/"}, method = RequestMethod.GET)
    public String indexPage(ModelMap model) throws DaoException {
        List<NewsDTO> news = newsService.findAll();
        model.addAttribute("news", news);
        return "index";
    }

    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String aboutUsPage(ModelMap model){
        return "about";
    }





}
