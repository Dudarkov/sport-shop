package gmail.alexdudarkov.sportshop.controllers;

import gmail.alexdudarkov.sportshop.service.model.LoginUserDTO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes(types = LoginUserDTO.class)
public class LoginController {

    @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
    public String showWelcomePage() {
        return "login";
    }
}
