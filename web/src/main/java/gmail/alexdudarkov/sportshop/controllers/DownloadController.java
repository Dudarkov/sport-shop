package gmail.alexdudarkov.sportshop.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;
import java.nio.charset.Charset;


@Controller
public class DownloadController {

    private static final Logger logger = Logger.getLogger(DownloadController.class);

    @Autowired
    private Environment environment;


    @RequestMapping(value = {"/download/**/{folder}/{id}"}, method = RequestMethod.GET)
    public void download(HttpServletResponse response, @PathVariable Long id, @PathVariable String folder) throws IOException {

        File file = new File(environment.getProperty("upload.location")+environment.getProperty(folder+".image.folder")+id+".jpg");

        if (!file.exists()) {
            String errorMessage = "Sorry. The file you are looking for does not exist";
            logger.info(errorMessage);
            OutputStream outputStream = response.getOutputStream();
            outputStream.write(errorMessage.getBytes(Charset.forName("UTF-8")));
            outputStream.close();
            return;
        }

        String mimeType = URLConnection.guessContentTypeFromName(file.getName());
        if (mimeType == null) {
            logger.info("Mime type is not detectable, will take default");
            mimeType = "application/octet-stream";
        }

        logger.info("Mime type : " + mimeType);
        response.setContentType(mimeType);

        response.setHeader("Content-Disposition", String.format("inline; filename=\"%s\"", file.getName()));
        response.setContentLength((int) file.length());

        try (
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStream inputStream = new BufferedInputStream(fileInputStream)
        ) {
            //Copy bytes from source to destination(outputstream in this example), closes both streams.
            FileCopyUtils.copy(inputStream, response.getOutputStream());
        }
    }
}
