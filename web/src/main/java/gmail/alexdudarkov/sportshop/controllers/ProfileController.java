package gmail.alexdudarkov.sportshop.controllers;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.UserService;
import gmail.alexdudarkov.sportshop.service.exception.ServiceException;
import gmail.alexdudarkov.sportshop.service.model.AppUserPrincipal;
import gmail.alexdudarkov.sportshop.service.model.ChangePasswordDTO;
import gmail.alexdudarkov.sportshop.service.model.UserInformationDTO;
import gmail.alexdudarkov.sportshop.validators.ChangePasswordValidator;
import gmail.alexdudarkov.sportshop.validators.UserInformationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/user")
public class ProfileController {
    private final ChangePasswordValidator changePasswordValidator;
    private final UserService userService;
    private final UserInformationValidator userInformationValidator;

    @Autowired
    public ProfileController(ChangePasswordValidator changePasswordValidator, UserService userService, UserInformationValidator userInformationValidator) {
        this.changePasswordValidator = changePasswordValidator;
        this.userService = userService;
        this.userInformationValidator = userInformationValidator;
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profileForm(ModelMap modelMap) throws ServiceException, DaoException {
        AppUserPrincipal authentication = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        Long id = authentication.getUserId();
        userService.findById(id);
        modelMap.addAttribute("user", userService.findById(id));
        return "user/profile";
    }

    @RequestMapping(value = "/change/password", method = RequestMethod.GET)
    public String changePasswordForm(@ModelAttribute(value = "changePassword") ChangePasswordDTO changePasswordDTO, ModelMap modelMap) {

        return "user/changePassword";
    }

    @RequestMapping(value = "/change/password", method = RequestMethod.POST)
    public String changePassword(@ModelAttribute(value = "changePassword") ChangePasswordDTO changePasswordDTO, BindingResult result, ModelMap modelMap) throws DaoException {
        changePasswordValidator.validate(changePasswordDTO, result);

        if (!result.hasErrors()) {
            userService.changePassword(changePasswordDTO);
            return "redirect:/user/profile";
        } else {
            return "user/changePassword";
        }
    }

    @RequestMapping(value = "/change/information", method = RequestMethod.GET)
    public String changeInformationForm(ModelMap modelMap) throws DaoException {
       UserInformationDTO userInformation=userService.getUserInformation();
       modelMap.addAttribute("userInformation", userInformation);
        return "user/changeInformation";
    }

    @RequestMapping(value = "/change/information", method = RequestMethod.POST)
    public String changeInformation(@ModelAttribute(value = "userInformation") UserInformationDTO userInformation, BindingResult result) throws DaoException {
        userInformationValidator.validate(userInformation, result);
        if (!result.hasErrors()) {
            userService.changeUserInformation(userInformation);
            return "redirect:user/profile";
        } else {
            return "user/changeInformation";
        }
    }
}
