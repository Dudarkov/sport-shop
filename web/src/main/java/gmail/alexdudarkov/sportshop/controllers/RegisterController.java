package gmail.alexdudarkov.sportshop.controllers;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.UserService;
import gmail.alexdudarkov.sportshop.service.model.RegistrationUserDTO;
import gmail.alexdudarkov.sportshop.validators.UserValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Properties;

@Controller
public class RegisterController {
    private static final Logger logger = Logger.getLogger(RegisterController.class);

    private final UserService userService;
    private final Properties properties;
    private final UserValidator userValidator;

    @Autowired
    public RegisterController(
            UserService userService,
            Properties properties,
            UserValidator userValidator
    ) {
        this.userService = userService;
        this.properties = properties;
        this.userValidator = userValidator;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String registrationPage(@ModelAttribute("user") RegistrationUserDTO user, ModelMap model) {
        logger.debug("Register page");
        return "reg";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerUser(@ModelAttribute("user") RegistrationUserDTO user, BindingResult result) throws DaoException {
        userValidator.validate(user, result);
        if (!result.hasErrors()) {
            userService.registerUser(user);
            return "redirect:/login";
        } else {
            return "reg";
        }
    }
}
