package gmail.alexdudarkov.sportshop.controllers.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/admin")
public class AdminMenuController {
    @RequestMapping(value = {"/adminMenu"}, method = RequestMethod.GET)
    public String catalogPage( ModelMap model) {

        return "administration/adminMenu";
    }
}
