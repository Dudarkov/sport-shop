package gmail.alexdudarkov.sportshop.controllers;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.NewsService;
import gmail.alexdudarkov.sportshop.service.model.NewsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.List;

@Controller
public class NewsController {

    private final NewsService newsService;

    @Autowired
    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @RequestMapping(value = "/news/list", method = RequestMethod.GET)
    public String getAllComments(ModelMap model) throws DaoException {
        List<NewsDTO> news = newsService.findAll();
        model.addAttribute("news", news);
        return "news";
    }

}
