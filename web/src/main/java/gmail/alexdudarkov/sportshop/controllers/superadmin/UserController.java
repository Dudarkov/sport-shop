package gmail.alexdudarkov.sportshop.controllers.superadmin;


import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.model.Role;
import gmail.alexdudarkov.sportshop.model.UserStatus;
import gmail.alexdudarkov.sportshop.service.UserService;
import gmail.alexdudarkov.sportshop.service.exception.ServiceException;
import gmail.alexdudarkov.sportshop.service.model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.List;

@Controller
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/superadmin/user/list", method = RequestMethod.GET)
    public String getAllUsers(ModelMap modelMap) throws DaoException {
        List<UserDTO> users = userService.getAll();
        modelMap.addAttribute("users", users);
        return "administration/superadminUserList";
    }

    @RequestMapping(value = "/superadmin/user/change/password", method = RequestMethod.GET)
    public String changePasswordForm(@RequestParam("id") Long id, ModelMap model) throws ServiceException, DaoException {

        UserDTO user = userService.findById(id);
        model.addAttribute("user", user);
        return "administration/changeUserPassword";

    }

    @RequestMapping(value = "/superadmin/user/change/password", method = RequestMethod.POST)
    public String changePassword(@ModelAttribute(value = "user") UserDTO user) throws ServiceException, DaoException {
        userService.changePassword(user.getId(), user.getPassword());
        return "redirect:/superadmin/user/list";

    }

    @RequestMapping(value = "/superadmin/user/change/role", method = RequestMethod.GET)
    public String changeRoleForm(@RequestParam("id") Long id, ModelMap model) throws ServiceException, DaoException {
        UserDTO user = userService.findById(id);
        List<Role> roles= Arrays.asList(Role.values());
        model.addAttribute("roles", roles);
        model.addAttribute("user", user);
        return "administration/changeUserRole";
    }

    @RequestMapping(value = "/superadmin/user/change/role", method = RequestMethod.POST)
    public String changeRole(@ModelAttribute(value = "user") UserDTO user) throws ServiceException, DaoException {
        userService.changeRole(user.getId(), user.getRole());
        return "redirect:/superadmin/user/list";

    }

    @RequestMapping(value = "/superadmin/user/change/status", method = RequestMethod.GET)
    public String changeStatusForm(@RequestParam("id") Long id, ModelMap model) throws ServiceException, DaoException {
        UserDTO user = userService.findById(id);
        List<UserStatus> statuses= Arrays.asList(UserStatus.values());
        model.addAttribute("user", user);
        model.addAttribute("statuses", statuses);
        return "administration/changeUserStatus";
    }

    @RequestMapping(value = "/superadmin/user/change/status", method = RequestMethod.POST)
    public String changePasswordForm(@ModelAttribute(value = "user") UserDTO user) throws ServiceException, DaoException {
        userService.changeStatus(user.getId(), user.getStatus());
        return "redirect:/superadmin/user/list";

    }


}
