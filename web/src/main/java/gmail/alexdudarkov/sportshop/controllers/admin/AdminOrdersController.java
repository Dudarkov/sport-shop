package gmail.alexdudarkov.sportshop.controllers.admin;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.model.Status;
import gmail.alexdudarkov.sportshop.service.OrderItemService;
import gmail.alexdudarkov.sportshop.service.OrderService;
import gmail.alexdudarkov.sportshop.service.model.OrderDTO;
import gmail.alexdudarkov.sportshop.service.model.OrderItemDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Arrays;
import java.util.List;

@Controller
public class AdminOrdersController {
    private final OrderItemService orderItemService;
    private final OrderService orderService;

    public AdminOrdersController(OrderItemService orderItemService, OrderService orderService) {
        this.orderItemService = orderItemService;
        this.orderService = orderService;
    }

    @RequestMapping( value = "admin/order/list", method = RequestMethod.GET)
    public String orderListForm(ModelMap modelMap) throws DaoException {
       List<OrderDTO> orders= orderService.getAll();
       modelMap.addAttribute("orders", orders);
        return "administration/adminOrderList";
    }

    @RequestMapping(value = "admin/order/change/status", method = RequestMethod.GET)
    public String changeStatusForm(@RequestParam(value = "id") Long id, ModelMap modelMap) throws DaoException {
        OrderDTO order=orderService.getById(id);

        modelMap.addAttribute("order", order);
        List<Status> statuses= Arrays.asList(Status.values());
        List<OrderItemDTO> orderItems= orderItemService.getByOrderId(id);
        modelMap.addAttribute("orderItems",orderItems);
        modelMap.addAttribute("statuses", statuses );
        return "administration/changeOrderStatus";
    }

    @RequestMapping(value = "admin/order/change/status", method = RequestMethod.POST)
    public String changeStatus(@ModelAttribute(value = "order") OrderDTO order) throws DaoException {
        Long orderId= order.getId();
        Status status=order.getStatus();
        orderService.changeStatus(orderId, status);
        return  "redirect:/admin/order/list";
    }



}
