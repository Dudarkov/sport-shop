package gmail.alexdudarkov.sportshop.controllers;

import gmail.alexdudarkov.sportshop.model.Role;
import gmail.alexdudarkov.sportshop.service.model.AppUserPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class DefaultSuccessController {

    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    public String filterDirectedUrl() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        if (principal.getRole() == Role.ROLE_USER) {
            return "redirect:/catalog?page=1";
        } else if (principal.getRole() == Role.ROLE_ADMIN) {
            return "redirect:/admin/adminMenu";
        } else {
            return "redirect:/login";
        }
    }
}