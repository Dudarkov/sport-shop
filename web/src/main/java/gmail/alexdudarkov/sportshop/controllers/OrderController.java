package gmail.alexdudarkov.sportshop.controllers;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.OrderItemService;
import gmail.alexdudarkov.sportshop.service.OrderService;
import gmail.alexdudarkov.sportshop.service.model.OrderDTO;
import gmail.alexdudarkov.sportshop.service.model.OrderItemDTO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller(value = "/order")
public class OrderController {

   private final OrderService orderService;
   private final OrderItemService orderItemService;

    public OrderController(OrderService orderService, OrderItemService orderItemService) {
        this.orderService = orderService;
        this.orderItemService = orderItemService;
    }

    @RequestMapping(value = "/order/add", method = RequestMethod.POST)
    public String addOrder(ModelMap modelMap) throws DaoException {
       orderService.createOrderFromBasket();

        return "user/order";
    }

    @RequestMapping(value = "/order/list", method = RequestMethod.GET)
    public String showOrdersPage(ModelMap modelMap){
       List<OrderDTO> orderDTOS= orderService.getUserOrders();
        modelMap.addAttribute("orders", orderDTOS);
        return "user/order";
    }

    @RequestMapping(value = "/order/items", method = RequestMethod.GET)
    public String showOrderItemList(@RequestParam Long id, ModelMap modelMap) throws DaoException {

        if (orderService.checkAttachmentUser(id)) {
            OrderDTO order = orderService.getById(id);
            List<OrderItemDTO> orderItems = orderItemService.getByOrderId(id);
            modelMap.addAttribute("orderItems", orderItems);
            modelMap.addAttribute("order", order);
            return "user/orderItems";
        } else {
            return "redirect:/order/list";
        }
    }
}
