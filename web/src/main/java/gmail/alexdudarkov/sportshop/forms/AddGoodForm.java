package gmail.alexdudarkov.sportshop.forms;

import gmail.alexdudarkov.sportshop.service.model.GoodDTO;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by User-PC on 04.08.2017.
 */
public class AddGoodForm {
    private GoodDTO goodDTO=new GoodDTO();
    private File file;
    private Map<String, String> errors = new HashMap<String, String>();
    private Map<String, String> messages = new HashMap<String, String>();

    public GoodDTO getGoodDTO() {
        return goodDTO;
    }

    public void setGoodDTO(GoodDTO goodDTO) {
        this.goodDTO = goodDTO;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public Map<String, String> getMessages() {
        return messages;
    }

    public void setError(String fieldName, String message) {
        errors.put(fieldName, message);
    }

    public void setMessage(String fieldName, String message) {
        messages.put(fieldName, message);
    }

    public boolean hasErrors() {
        return errors.size() > 0;
    }

}
