package gmail.alexdudarkov.sportshop.validators;

import gmail.alexdudarkov.sportshop.service.model.AppUserPrincipal;
import gmail.alexdudarkov.sportshop.service.model.ChangePasswordDTO;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ChangePasswordValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(ChangePasswordDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ChangePasswordDTO changePasswordDTO =(ChangePasswordDTO)o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "error.password.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassword", "error.password.empty");
        AppUserPrincipal authentication = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        String password=authentication.getPassword();
     //   authentication.
        if (!changePasswordDTO.getNewPassword().contentEquals(changePasswordDTO.getRepeatNewPassword())){
            errors.rejectValue("repeatNewPassword", "error.password.not_equals");
        }

        if(!BCrypt.checkpw(changePasswordDTO.getPassword(), password)){
            errors.rejectValue("password","error.password.wrong");
        }
    }
}
