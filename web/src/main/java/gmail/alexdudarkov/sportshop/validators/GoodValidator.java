package gmail.alexdudarkov.sportshop.validators;

import gmail.alexdudarkov.sportshop.service.model.GoodDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class GoodValidator  implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(GoodDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        GoodDTO goodDTO=(GoodDTO)o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "model", "error.good.model.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "type", "error.good.type.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "brand", "error.good.brand.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "error.good.price.empty");

        if (goodDTO.getFile().isEmpty()){
            errors.rejectValue("file", "error.good.file.empty");
        }
    }
}
