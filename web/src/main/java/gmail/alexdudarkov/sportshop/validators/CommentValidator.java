package gmail.alexdudarkov.sportshop.validators;


import gmail.alexdudarkov.sportshop.service.model.CommentDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class CommentValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(CommentDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "error.comment.content.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "content", "error.comment.content.empty");
    }
}
