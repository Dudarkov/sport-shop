package gmail.alexdudarkov.sportshop.validators;

import gmail.alexdudarkov.sportshop.service.model.UserInformationDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserInformationValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UserInformationDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
UserInformationDTO userInformation=(UserInformationDTO)o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "error.name.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "error.surname.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "error.phone.empty");
    }
}
