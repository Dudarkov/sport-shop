package gmail.alexdudarkov.sportshop.validators;

import gmail.alexdudarkov.sportshop.service.model.NewsDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class NewsValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(NewsDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
NewsDTO newsDTO=(NewsDTO)o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "error.news.title.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "text", "error.news.text.empty");
        ValidationUtils.rejectIfEmpty(errors, "dateNews", "error.news.date.empty");
        if (newsDTO.getFile().isEmpty()){
            errors.rejectValue("file", "error.news.file.empty");
        }
    }
}
