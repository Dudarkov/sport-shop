package gmail.alexdudarkov.sportshop.service.impl;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.dao.UserDao;
import gmail.alexdudarkov.sportshop.model.Role;
import gmail.alexdudarkov.sportshop.model.User;
import gmail.alexdudarkov.sportshop.model.UserInformation;
import gmail.alexdudarkov.sportshop.model.UserStatus;
import gmail.alexdudarkov.sportshop.service.UserService;
import gmail.alexdudarkov.sportshop.service.converters.LoginConverter;
import gmail.alexdudarkov.sportshop.service.converters.RegistrationConverter;
import gmail.alexdudarkov.sportshop.service.converters.UserConverter;
import gmail.alexdudarkov.sportshop.service.converters.UserInformationConverter;
import gmail.alexdudarkov.sportshop.service.exception.ServiceException;
import gmail.alexdudarkov.sportshop.service.model.*;
import gmail.alexdudarkov.sportshop.service.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {


    private final UserDao userDao;
    private final PasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(
            UserDao userDao,
            PasswordEncoder bCryptPasswordEncoder
    ) {
        this.userDao = userDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    @Transactional
    public LoginUserDTO get(LoginUserDTO loginUserDTO) throws DaoException {

        User u = LoginConverter.convertIntoDao(loginUserDTO);
        List<User> users = userDao.findAllByExample(u);


        if (!users.isEmpty()) {
            LoginUserDTO loginUserDTO1 = LoginConverter.convertIntoDto(users.get(0));
            return loginUserDTO1;
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public UserDTO findById(Serializable id) throws ServiceException, DaoException {
        UserDTO userDto = UserConverter.convert(userDao.findById(id));
        return userDto;
    }

    @Override
    @Transactional
    public Long registerUser(RegistrationUserDTO registrationUserDTO) throws DaoException {
        User user = RegistrationConverter.convertIntoDao(registrationUserDTO);
        user.setPassword(bCryptPasswordEncoder.encode(registrationUserDTO.getPassword()));
        Long id = (Long) userDao.save(user);
        return id;
    }

    @Override
    @Transactional
    public Long save(UserDTO userDto) throws DaoException {
        Long id = (Long) userDao.save(UserConverter.convert(userDto));
        return id;
    }

    @Override
    @Transactional
    public void changePassword(ChangePasswordDTO changePasswordDTO) throws DaoException {
        AppUserPrincipal authentication = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        Long id = authentication.getUserId();
        User user = userDao.findById(id);
        user.setPassword(bCryptPasswordEncoder.encode(changePasswordDTO.getNewPassword()));
        userDao.saveOrUpdate(user);
    }

    @Transactional
    @Override
    public void changePassword(Long userId, String newPassword) throws DaoException {
        User user = userDao.findById(userId);
        String password = bCryptPasswordEncoder.encode(newPassword);
        user.setPassword(password);
        userDao.saveOrUpdate(user);
    }

    @Transactional
    @Override
    public void changeRole(Long userId, Role role) throws DaoException {
        User user = userDao.findById(userId);
        user.setRole(role);
        userDao.saveOrUpdate(user);
    }

    @Transactional
    @Override
    public void changeStatus(Long userId, UserStatus status) throws DaoException {
        User user = userDao.findById(userId);
        user.setUserStatus(status);
        userDao.saveOrUpdate(user);
    }

    @Transactional
    @Override
    public User findByLogin(String login) {
        User user = userDao.findByLogin(login);
        return user;

    }

    @Transactional
    @Override
    public UserInformationDTO getUserInformation() throws DaoException {
        Long idUser = UserUtil.getUserId();
        User user = userDao.findById(idUser);
        UserInformation userInformation = user.getUserInformation();
        UserInformationDTO userInformationDTO = UserInformationConverter.convert(userInformation);
        return userInformationDTO;
    }

    @Transactional
    @Override
    public void changeUserInformation(UserInformationDTO userInformationDTO) throws DaoException {

        Long idUser = UserUtil.getUserId();
        User user = userDao.findById(idUser);
        user.getUserInformation().setName(userInformationDTO.getName());
        user.getUserInformation().setSurname(userInformationDTO.getSurname());
        user.getUserInformation().setPhone(userInformationDTO.getPhone());
        userDao.saveOrUpdate(user);
    }

    @Transactional
    @Override
    public List<UserDTO> getAll() throws DaoException {
        List<User> users = userDao.findAll();
        return UserConverter.convert(users);
    }
}
