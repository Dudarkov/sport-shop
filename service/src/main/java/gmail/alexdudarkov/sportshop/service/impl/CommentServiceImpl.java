package gmail.alexdudarkov.sportshop.service.impl;

import gmail.alexdudarkov.sportshop.dao.CommentDao;
import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.model.Comment;
import gmail.alexdudarkov.sportshop.service.CommentService;
import gmail.alexdudarkov.sportshop.service.converters.CommentConverter;
import gmail.alexdudarkov.sportshop.service.model.CommentDTO;
import gmail.alexdudarkov.sportshop.service.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private final CommentDao commentDao;

    public CommentServiceImpl(CommentDao commentDao) {
        this.commentDao = commentDao;
    }

    @Override
    @Transactional
    public void saveComment(CommentDTO commentDTO) throws DaoException {
        Long userId = UserUtil.getUserId();
        commentDTO.setUserId(userId);
        Comment comment = CommentConverter.convert(commentDTO);
        commentDao.saveOrUpdate(comment);

    }
}
