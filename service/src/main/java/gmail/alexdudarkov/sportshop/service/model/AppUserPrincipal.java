package gmail.alexdudarkov.sportshop.service.model;


import gmail.alexdudarkov.sportshop.model.Role;
import gmail.alexdudarkov.sportshop.model.User;
import gmail.alexdudarkov.sportshop.model.UserStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class AppUserPrincipal implements UserDetails{
    private User user;
    private Collection<SimpleGrantedAuthority> grantedAuthorities;

    public AppUserPrincipal(User user) {
        this.user = user;
        grantedAuthorities = Collections.singletonList(
                new SimpleGrantedAuthority(user.getRole().name())
        );
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getUserId() {
        return user.getId();
    }

    public Role getRole() {
        return user.getRole();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.getUserStatus().equals(UserStatus.ENABLE);
    }


}
