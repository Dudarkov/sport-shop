package gmail.alexdudarkov.sportshop.service.util;

import gmail.alexdudarkov.sportshop.service.model.AppUserPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserUtil {


    public static Long getUserId(){
        AppUserPrincipal authentication = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        Long id = authentication.getUserId();
        return id;
    }
}
