package gmail.alexdudarkov.sportshop.service;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.model.Role;
import gmail.alexdudarkov.sportshop.model.User;
import gmail.alexdudarkov.sportshop.model.UserStatus;
import gmail.alexdudarkov.sportshop.service.exception.ServiceException;
import gmail.alexdudarkov.sportshop.service.model.*;

import java.io.Serializable;
import java.util.List;


public interface UserService {
    LoginUserDTO get(LoginUserDTO loginUserDTO) throws DaoException;

    UserDTO findById(Serializable id) throws ServiceException, DaoException;

    Long save(UserDTO userDto) throws ServiceException, DaoException;

    Long registerUser(RegistrationUserDTO registrationUserDTO) throws DaoException;

    User findByLogin(String login);

    UserInformationDTO getUserInformation() throws DaoException;

    void changePassword(ChangePasswordDTO changePasswordDTO) throws DaoException;

    void changeUserInformation(UserInformationDTO userInformationDTO) throws DaoException;

    List<UserDTO> getAll() throws DaoException;

    void changePassword(Long userId, String newPassword) throws DaoException;

    void changeRole(Long userId, Role role) throws DaoException;

    void changeStatus(Long userId, UserStatus status) throws DaoException;
}
