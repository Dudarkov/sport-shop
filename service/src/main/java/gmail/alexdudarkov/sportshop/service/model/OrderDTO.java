package gmail.alexdudarkov.sportshop.service.model;


import gmail.alexdudarkov.sportshop.model.Status;

import java.math.BigDecimal;
import java.util.Date;

public class OrderDTO {
    private Long id;

    private Status status;

    private Date date;

    private UserDTO user;

    private BigDecimal totalPrice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "OrderDTO{" +
                "id=" + id +
                ", status=" + status +
                ", date=" + date +
                ", user=" + user +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
