package gmail.alexdudarkov.sportshop.service;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.exception.ServiceException;
import gmail.alexdudarkov.sportshop.service.model.BrandGoodDTO;

import java.io.Serializable;
import java.util.List;

public interface BrandGoodService {
    public List<BrandGoodDTO> findAll() throws ServiceException, DaoException;
    public Serializable save(BrandGoodDTO brandGoodDto) throws ServiceException, DaoException;
}
