package gmail.alexdudarkov.sportshop.service;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.model.NewsDTO;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

public interface NewsService {
    List<NewsDTO> findAll() throws DaoException;

    Serializable save(NewsDTO newsDto) throws DaoException;

    void delete(NewsDTO newsDTO) throws DaoException;

    NewsDTO findById(Long id) throws DaoException;

    void update(NewsDTO newsDTO) throws IOException, DaoException;
}
