package gmail.alexdudarkov.sportshop.service.impl;

import gmail.alexdudarkov.sportshop.dao.BasketItemDao;
import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.BasketItemService;
import gmail.alexdudarkov.sportshop.service.converters.BasketItemConverter;
import gmail.alexdudarkov.sportshop.service.model.BasketItemDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

@Service
public class BasketItemServiceImpl implements BasketItemService {

    @Autowired
    private BasketItemDao basketItemDao;

    @Override
    @Transactional
    public Serializable save(BasketItemDTO basketItemDto) throws DaoException {
        Long id = null;
        id = (Long) basketItemDao.save(BasketItemConverter.convert(basketItemDto));
        return id;
    }


}
