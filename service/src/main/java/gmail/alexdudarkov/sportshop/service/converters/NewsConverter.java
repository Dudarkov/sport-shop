package gmail.alexdudarkov.sportshop.service.converters;

import gmail.alexdudarkov.sportshop.model.News;
import gmail.alexdudarkov.sportshop.service.model.NewsDTO;

import java.util.ArrayList;
import java.util.List;


public class NewsConverter {

    public static News convert(NewsDTO newsDTO){
        News news=new News();
        news.setId(newsDTO.getId());
        news.setTitle(newsDTO.getTitle());
        news.setIconName(newsDTO.getIconName());
        news.setDateNews(newsDTO.getDateNews());
        news.setText(newsDTO.getText());
        return news;
    }

    public static NewsDTO convert(News news){
        NewsDTO newsDTO=new NewsDTO();
        newsDTO.setId(news.getId());
        newsDTO.setTitle(news.getTitle());
        newsDTO.setIconName(news.getIconName());
        newsDTO.setDateNews(news.getDateNews());
        newsDTO.setText(news.getText());
        return newsDTO;
    }

    public static List<NewsDTO> convert(List<News> newsList){
        List<NewsDTO> newsDTOList = new ArrayList<>();
        for (int i = 0; i < newsList.size(); i++) {
            NewsDTO goodDto = NewsConverter.convert(newsList.get(i));
            newsDTOList.add(goodDto);
        }
        return newsDTOList;
    }
}
