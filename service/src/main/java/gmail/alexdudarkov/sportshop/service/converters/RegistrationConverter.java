package gmail.alexdudarkov.sportshop.service.converters;

import gmail.alexdudarkov.sportshop.model.*;
import gmail.alexdudarkov.sportshop.service.model.RegistrationUserDTO;

public class RegistrationConverter {


    public static User convertIntoDao(RegistrationUserDTO registrationUserDTO) {
        User user = new User();
        UserInformation userInformation = new UserInformation();
        Basket basket = new Basket();
        user.setPassword(registrationUserDTO.getPassword());
        user.setUsername(registrationUserDTO.getUsername());
        user.setRole(Role.ROLE_USER);
        user.setUserStatus(UserStatus.ENABLE);
        userInformation.setName(registrationUserDTO.getName());
        userInformation.setSurname(registrationUserDTO.getSurname());
        userInformation.setPhone(registrationUserDTO.getPhone());
        user.setUserInformation(userInformation);
        user.setBasket(basket);
        userInformation.setUser(user);
        basket.setUser(user);
        return user;
    }

    public static RegistrationUserDTO convertIntoDto(User user) {
        RegistrationUserDTO registrationUserDTO = new RegistrationUserDTO();
        registrationUserDTO.setUsername(user.getUsername());
        registrationUserDTO.setPassword(user.getPassword());
        registrationUserDTO.setName(user.getUserInformation().getName());
        registrationUserDTO.setSurname(user.getUserInformation().getSurname());
        registrationUserDTO.setPhone(user.getUserInformation().getPhone());
        return registrationUserDTO;
    }
}
