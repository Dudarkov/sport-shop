package gmail.alexdudarkov.sportshop.service.converters;

import gmail.alexdudarkov.sportshop.model.User;
import gmail.alexdudarkov.sportshop.model.UserInformation;
import gmail.alexdudarkov.sportshop.service.model.UserDTO;

import java.util.ArrayList;
import java.util.List;


public class UserConverter {

    public static User convert(UserDTO userDto) {
        User user = new User();
        UserInformation userInformation = new UserInformation();
        user.setId(userDto.getId());
        user.setUsername(userDto.getLogin());
        user.setRole(userDto.getRole());
        user.setPassword(userDto.getPassword());
        userInformation.setName(userDto.getName());
        userInformation.setSurname(userDto.getSurname());
        userInformation.setPhone(userDto.getPhone());
        user.setUserInformation(userInformation);
        userInformation.setUser(user);
        user.setUserStatus(userDto.getStatus());
        return user;
    }

    public static UserDTO convert(User user){
        UserDTO userDTO=new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setRole(user.getRole());
        userDTO.setPassword(user.getPassword());
        userDTO.setLogin(user.getUsername());
        userDTO.setSurname(user.getUserInformation().getSurname());
        userDTO.setName(user.getUserInformation().getName());
        userDTO.setPhone(user.getUserInformation().getPhone());
        userDTO.setStatus(user.getUserStatus());
        return userDTO;
  }

    public static List<UserDTO> convert(List<User> userList) {
        List<UserDTO> userDTOList = new ArrayList<>();
        for (int i = 0; i < userList.size(); i++) {
            UserDTO userDto = UserConverter.convert(userList.get(i));
            userDTOList.add(userDto);
        }
        return userDTOList;
    }
}
