package gmail.alexdudarkov.sportshop.service.converters;


import gmail.alexdudarkov.sportshop.model.UserInformation;
import gmail.alexdudarkov.sportshop.service.model.UserInformationDTO;

public class UserInformationConverter {

    public static UserInformationDTO convert(UserInformation userInformation){
        UserInformationDTO userInformationDTO=new UserInformationDTO();
        userInformationDTO.setName(userInformation.getName());
        userInformationDTO.setPhone(userInformation.getPhone());
        userInformationDTO.setSurname(userInformation.getSurname());
        return  userInformationDTO;
    }
}
