package gmail.alexdudarkov.sportshop.service.converters;


import gmail.alexdudarkov.sportshop.model.TypeGood;
import gmail.alexdudarkov.sportshop.service.model.TypeGoodDTO;

public class TypeGoodConverter {

    public static TypeGoodDTO convertIntoDto(TypeGood typeGood){
        TypeGoodDTO typeGoodDTO=new TypeGoodDTO();
        typeGoodDTO.setId(typeGood.getId());
        typeGoodDTO.setName(typeGood.getName());
        return typeGoodDTO;
    }

    public static TypeGood convertIntoDao(TypeGoodDTO typeGoodDTO){
        TypeGood typeGood=new TypeGood();
        typeGood.setId(typeGoodDTO.getId());
        typeGood.setName(typeGoodDTO.getName());
        return typeGood;
    }
}
