package gmail.alexdudarkov.sportshop.service.model;

import java.util.List;

public class CatalogDTO {


    private List<TypeGoodDTO> typeGoodDTOS;
    private List<BrandGoodDTO> brandGoodDTOS;
    private List<BasketItemDTO> basketItemDTO;
    private Integer pagesCount;
    private Integer pageNumber;


    public List<TypeGoodDTO> getTypeGoodDTOS() {
        return typeGoodDTOS;
    }

    public void setTypeGoodDTOS(List<TypeGoodDTO> typeGoodDTOS) {
        this.typeGoodDTOS = typeGoodDTOS;
    }

    public List<BrandGoodDTO> getBrandGoodDTOS() {
        return brandGoodDTOS;
    }

    public void setBrandGoodDTOS(List<BrandGoodDTO> brandGoodDTOS) {
        this.brandGoodDTOS = brandGoodDTOS;
    }

    public Integer getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(Integer pagesCount) {
        this.pagesCount = pagesCount;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public List<BasketItemDTO> getBasketItemDTO() {
        return basketItemDTO;
    }

    public void setBasketItemDTO(List<BasketItemDTO> basketItemDTO) {
        this.basketItemDTO = basketItemDTO;
    }
}
