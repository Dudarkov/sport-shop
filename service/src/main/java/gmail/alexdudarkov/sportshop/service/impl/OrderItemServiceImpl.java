package gmail.alexdudarkov.sportshop.service.impl;

import gmail.alexdudarkov.sportshop.dao.OrderItemDao;
import gmail.alexdudarkov.sportshop.model.OrderItem;
import gmail.alexdudarkov.sportshop.service.OrderItemService;
import gmail.alexdudarkov.sportshop.service.converters.OrderItemConverter;
import gmail.alexdudarkov.sportshop.service.model.OrderItemDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Controller
public class OrderItemServiceImpl implements OrderItemService{

    private final OrderItemDao orderItemDao;

    @Autowired
    public OrderItemServiceImpl(OrderItemDao orderItemDao) {
        this.orderItemDao = orderItemDao;
    }

    @Transactional
    @Override
    public List<OrderItemDTO> getByOrderId(Long id){
       List<OrderItem> orderItems= orderItemDao.findByOrderId(id);
        return OrderItemConverter.convert(orderItems);
    }
}
