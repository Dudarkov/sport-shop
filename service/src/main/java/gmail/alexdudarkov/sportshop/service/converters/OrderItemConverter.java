package gmail.alexdudarkov.sportshop.service.converters;


import gmail.alexdudarkov.sportshop.model.OrderItem;
import gmail.alexdudarkov.sportshop.service.model.OrderItemDTO;

import java.util.ArrayList;
import java.util.List;

public class OrderItemConverter {

    public static OrderItemDTO convert(OrderItem orderItem){
        OrderItemDTO orderItemDTO=new OrderItemDTO();
        orderItemDTO.setId(orderItem.getId());
        orderItemDTO.setCount(orderItem.getCount());
        orderItemDTO.setPrice(orderItem.getPrice());
        orderItemDTO.setGood(GoodConverter.convertIntoDto(orderItem.getGood()));
        return orderItemDTO;
    }

    public static List<OrderItemDTO> convert(List<OrderItem> orderItems){
        List<OrderItemDTO> orderItemDTOS=new ArrayList<>();
        for (int i = 0; i < orderItems.size(); i++) {
            OrderItemDTO orderItemDto = convert(orderItems.get(i));
            orderItemDTOS.add(orderItemDto);
        }
        return orderItemDTOS;
    }
}
