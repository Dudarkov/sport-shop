package gmail.alexdudarkov.sportshop.service;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.model.BasketDTO;


public interface BasketService {
    BasketDTO getUserBasket() throws DaoException;

    void clearBasket(Long id);

    void deleteItemFromBasket(Long idItem) throws DaoException;
}
