package gmail.alexdudarkov.sportshop.service.converters;

import gmail.alexdudarkov.sportshop.model.BrandGood;
import gmail.alexdudarkov.sportshop.model.Good;
import gmail.alexdudarkov.sportshop.model.TypeGood;
import gmail.alexdudarkov.sportshop.service.model.BrandGoodDTO;
import gmail.alexdudarkov.sportshop.service.model.GoodDTO;
import gmail.alexdudarkov.sportshop.service.model.TypeGoodDTO;

import java.util.ArrayList;
import java.util.List;


public class GoodConverter {

    public static GoodDTO convertIntoDto(Good good) {
        GoodDTO goodDTO = new GoodDTO();
        BrandGoodDTO brandGoodDTO = new BrandGoodDTO();
        TypeGoodDTO typeGoodDTO = new TypeGoodDTO();
        goodDTO.setId(good.getId());
        goodDTO.setPrice(good.getPrice());
        goodDTO.setModel(good.getModel());
        goodDTO.setAvailability(good.getAvailability());
        goodDTO.setType(TypeGoodConverter.convertIntoDto(good.getTypeGood()));
        goodDTO.setBrand(BrandGoodConverter.convertIntoDto(good.getBrandGood()));
        goodDTO.setIconPath(good.getIconPath());
        return goodDTO;
    }

    public static Good convertIntoDao(GoodDTO goodDTO) {
        Good good = new Good();
        TypeGood typeGood = new TypeGood();
        BrandGood brandGood = new BrandGood();
        good.setId(goodDTO.getId());
        good.setPrice(goodDTO.getPrice());
        good.setModel(goodDTO.getModel());
        good.setIconPath(goodDTO.getIconPath());
        good.setAvailability(goodDTO.getAvailability());
        typeGood.setId(goodDTO.getType().getId());
        good.setTypeGood(typeGood);
        brandGood.setId(goodDTO.getBrand().getId());
        good.setBrandGood(brandGood);
        return good;
    }

    public static List<GoodDTO> convert(List<Good> goodList) {
        List<GoodDTO> goodDTOList = new ArrayList<>();
        for (int i = 0; i < goodList.size(); i++) {
            GoodDTO goodDto = GoodConverter.convertIntoDto(goodList.get(i));
            goodDTOList.add(goodDto);
        }
        return goodDTOList;
    }
}
