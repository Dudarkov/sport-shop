package gmail.alexdudarkov.sportshop.service.impl;


import gmail.alexdudarkov.sportshop.dao.BasketDao;
import gmail.alexdudarkov.sportshop.dao.BasketItemDao;
import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.model.BasketItem;
import gmail.alexdudarkov.sportshop.service.BasketService;
import gmail.alexdudarkov.sportshop.service.converters.BasketConverter;
import gmail.alexdudarkov.sportshop.service.model.BasketDTO;
import gmail.alexdudarkov.sportshop.service.util.UserUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BasketServiceImpl implements BasketService {

    private final BasketDao basketDao;
    private final BasketItemDao basketItemDao;

    public BasketServiceImpl(BasketDao basketDao, BasketItemDao basketItemDao) {
        this.basketDao = basketDao;
        this.basketItemDao = basketItemDao;
    }

    @Transactional
    @Override
    public BasketDTO getUserBasket() throws DaoException {
        Long id=UserUtil.getUserId();
        return BasketConverter.convert(basketDao.findById(id));
    }

    @Transactional
    @Override
    public void clearBasket(Long id) {
        basketDao.clearBasket(id);
    }

    @Transactional
    @Override
   public void deleteItemFromBasket(Long idItem) throws DaoException {
       BasketItem basketItem= basketItemDao.findById(idItem);
       Long basketId=basketItem.getBasket().getId();
       Long userId=UserUtil.getUserId();
       if(basketId.equals(userId)){
           basketItemDao.delete(basketItem);
       }
   }
}
