package gmail.alexdudarkov.sportshop.service.converters;

import gmail.alexdudarkov.sportshop.service.model.BasketItemDTO;
import gmail.alexdudarkov.sportshop.service.model.BrandGoodDTO;
import gmail.alexdudarkov.sportshop.service.model.CatalogDTO;
import gmail.alexdudarkov.sportshop.service.model.TypeGoodDTO;

import java.util.List;


public class CatalogConverter {

    public static CatalogDTO convertIntoDTO(List<BasketItemDTO> basketItemDTOS, List<BrandGoodDTO> brandGoodDTOS, List<TypeGoodDTO> typeGoodDTOS, Integer pageCount, Integer pageNumber) {
        CatalogDTO catalogDTO=new CatalogDTO();
        catalogDTO.setBrandGoodDTOS(brandGoodDTOS);
        catalogDTO.setTypeGoodDTOS(typeGoodDTOS);
        catalogDTO.setBasketItemDTO(basketItemDTOS);
        catalogDTO.setPageNumber(pageNumber);
        catalogDTO.setPagesCount(pageCount);
        return catalogDTO;
    }
}
