package gmail.alexdudarkov.sportshop.service.impl;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.dao.NewsDao;
import gmail.alexdudarkov.sportshop.model.News;
import gmail.alexdudarkov.sportshop.service.NewsService;
import gmail.alexdudarkov.sportshop.service.converters.NewsConverter;
import gmail.alexdudarkov.sportshop.service.model.NewsDTO;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;


@Service
public class NewsServiceImpl implements NewsService {

    private static final Logger logger = Logger.getLogger(GoodServiceImpl.class);
    private final NewsDao newsDao;
    @Autowired
    private Environment environment;

    public NewsServiceImpl(NewsDao newsDao) {
        this.newsDao = newsDao;
    }

    @Override
    @Transactional
    public List<NewsDTO> findAll() throws DaoException {
        List<News> newsList = newsDao.findAll();
        return NewsConverter.convert(newsList);
    }

    @Override
    @Transactional
    public Serializable save(NewsDTO newsDto) throws DaoException {
        Long id = null;
        try {
            id = (Long) newsDao.save(NewsConverter.convert(saveImage(newsDto)));
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new DaoException();
        }
        return id;
    }

    @Override
    @Transactional
    public void update(NewsDTO newsDTO) throws IOException, DaoException {
        if(deleteImage(newsDTO)) {
            newsDTO = saveImage(newsDTO);
        }
       News news= NewsConverter.convert(newsDTO);
        newsDao.saveOrUpdate(news);

    }

    private NewsDTO saveImage(NewsDTO newsDTO) throws IOException {
        String imageName = System.currentTimeMillis() + "";
        String fileLocation = environment.getProperty("upload.location") + environment.getProperty("news.image.folder") + imageName + ".jpg";
        FileCopyUtils.copy(newsDTO.getFile().getBytes(), new File(fileLocation));
        newsDTO.setIconName(imageName);
        return newsDTO;
    }

    private boolean deleteImage(NewsDTO newsDTO) {
        String imageName = newsDTO.getIconName();
        File file = new File(environment.getProperty("upload.location") + environment.getProperty("news.image.folder") + imageName + ".jpg");
        return file.delete();
    }

    @Override
    @Transactional
    public void delete(NewsDTO newsDTO) throws DaoException {
        newsDao.delete(NewsConverter.convert(newsDTO));
        deleteImage(newsDTO);
    }

    @Override
    @Transactional
    public NewsDTO findById(Long id) throws DaoException {
        News news = newsDao.findById(id);
        NewsDTO newsDTO = null;
        if (news != null) {
            newsDTO = NewsConverter.convert(news);
        }
        return newsDTO;
    }
}
