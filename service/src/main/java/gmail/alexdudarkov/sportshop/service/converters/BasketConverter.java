package gmail.alexdudarkov.sportshop.service.converters;


import gmail.alexdudarkov.sportshop.model.Basket;
import gmail.alexdudarkov.sportshop.model.BasketItem;
import gmail.alexdudarkov.sportshop.service.model.BasketDTO;
import gmail.alexdudarkov.sportshop.service.model.BasketItemDTO;

import java.math.BigDecimal;
import java.util.List;

public class BasketConverter {

    public static BasketDTO convert(Basket basket){
        BasketDTO basketDTO=new BasketDTO();
        basketDTO.setTotalPrice(getTotalPrice(basket));
        basketDTO.setId(basket.getId());
        basketDTO.setUserId(basket.getId());
        basketDTO.setBasketItems(BasketItemConverter.convert(basket.getBasketItems()));
        return basketDTO;
    }

    private static BigDecimal getTotalPrice(Basket basket){
        List<BasketItem> basketItemList= basket.getBasketItems();
        BigDecimal totalPrice=new BigDecimal("0");
        for (int i = 0; i < basketItemList.size(); i++) {
            BigDecimal price=basketItemList.get(i).getPrice();
            BigDecimal count= new BigDecimal(basketItemList.get(i).getCount());
            totalPrice=totalPrice.add(price.multiply(count));
        }
        return totalPrice;
    }
}
