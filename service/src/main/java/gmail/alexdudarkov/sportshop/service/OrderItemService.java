package gmail.alexdudarkov.sportshop.service;


import gmail.alexdudarkov.sportshop.service.model.OrderItemDTO;

import java.util.List;

public interface OrderItemService {
    List<OrderItemDTO> getByOrderId(Long id);
}
