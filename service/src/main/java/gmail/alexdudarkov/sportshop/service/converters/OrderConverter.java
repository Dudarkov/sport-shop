package gmail.alexdudarkov.sportshop.service.converters;

import gmail.alexdudarkov.sportshop.model.*;
import gmail.alexdudarkov.sportshop.service.model.OrderDTO;
import gmail.alexdudarkov.sportshop.service.model.UserDTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class OrderConverter {

    public static Order convert(Basket basket) {
        List<BasketItem> basketItemList = basket.getBasketItems();
        List<OrderItem> orderItemList = new ArrayList<>();
        Order order = new Order();
        for (int i = 0; i < basketItemList.size(); i++) {
            OrderItem orderItem = convert(basketItemList.get(i));
            orderItemList.add(orderItem);
            orderItem.setOrder(order);
        }

        order.setOrderItems(orderItemList);
        return order;
    }

    public static OrderItem convert(BasketItem basketItem) {
        OrderItem orderItem = new OrderItem();
        orderItem.setCount(basketItem.getCount());
        orderItem.setPrice(basketItem.getPrice());
        orderItem.setGood(basketItem.getGood());
        return orderItem;
    }

    public static OrderDTO convert(Order order){
        OrderDTO orderDTO=new OrderDTO();
        orderDTO.setDate(order.getDate());
        orderDTO.setId(order.getId());
        orderDTO.setStatus(order.getStatus());
        orderDTO.setTotalPrice(getTotalPrice(order));
        User user=order.getUser();
        UserDTO userDTO=UserConverter.convert(user);
        orderDTO.setUser(userDTO);
        return orderDTO;
    }

    public static List<OrderDTO> convert(List<Order> orderList){
        List<OrderDTO> orderDTOList=new ArrayList<>();
        for (int i = 0; i < orderList.size(); i++) {
            OrderDTO orderDto = OrderConverter.convert(orderList.get(i));
            orderDTOList.add(orderDto);
        }
        return orderDTOList;
    }

    private static BigDecimal getTotalPrice(Order order){
        List<OrderItem> orderItems= order.getOrderItems();
        BigDecimal totalPrice=new BigDecimal("0");
        for (int i = 0; i < orderItems.size(); i++) {
            BigDecimal price=orderItems.get(i).getPrice();
            BigDecimal count= new BigDecimal(orderItems.get(i).getCount());
            totalPrice=totalPrice.add(price.multiply(count));
        }
        return totalPrice;
    }
}
