package gmail.alexdudarkov.sportshop.service.model;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BasketDTO {
    private Long id;
    private Long userId;
    private BigDecimal totalPrice;
    private List<BasketItemDTO> basketItems = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<BasketItemDTO> getBasketItems() {
        return basketItems;
    }

    public void setBasketItems(List<BasketItemDTO> basketItems) {
        this.basketItems = basketItems;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "BasketDTO{" +
                "id=" + id +
                ", userId=" + userId +
                ", basketItems=" + basketItems +
                '}';
    }
}
