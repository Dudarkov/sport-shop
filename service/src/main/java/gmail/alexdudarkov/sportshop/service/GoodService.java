package gmail.alexdudarkov.sportshop.service;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.exception.ServiceException;
import gmail.alexdudarkov.sportshop.service.model.GoodDTO;

import java.io.Serializable;
import java.util.List;


public interface GoodService {
    public GoodDTO getById(Long id) throws DaoException;
    public Serializable save(GoodDTO goodDto) throws ServiceException, DaoException;
    public List<GoodDTO> findAll() throws ServiceException, DaoException;
    public List<GoodDTO> getSome(Integer pageNumber, Integer count);
    public Long getCountOfPage(Integer countOnPage);
}
