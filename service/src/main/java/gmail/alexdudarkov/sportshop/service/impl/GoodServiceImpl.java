package gmail.alexdudarkov.sportshop.service.impl;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.dao.GoodDao;
import gmail.alexdudarkov.sportshop.model.Good;
import gmail.alexdudarkov.sportshop.service.GoodService;
import gmail.alexdudarkov.sportshop.service.converters.GoodConverter;
import gmail.alexdudarkov.sportshop.service.model.GoodDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;

@Service
public class GoodServiceImpl implements GoodService {


    private static final Logger logger = Logger.getLogger(GoodServiceImpl.class);
    private final GoodDao goodDao;
    @Autowired
    private Environment environment;

    @Autowired
    public GoodServiceImpl(GoodDao goodDao) {
        this.goodDao = goodDao;
    }

    @Override
    @Transactional
    public GoodDTO getById(Long id) throws DaoException {

        return GoodConverter.convertIntoDto(goodDao.findById(id));
    }

    @Override
    @Transactional
    public Serializable save(GoodDTO goodDto) throws DaoException {
        Long id = null;
        try {
            id = (Long) goodDao.save(GoodConverter.convertIntoDao(saveImage(goodDto)));
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new DaoException();
        }
        return id;
    }

    @Override
    @Transactional
    public List<GoodDTO> findAll() throws DaoException {

        List<Good> goodList = goodDao.findAll();
        return GoodConverter.convert(goodList);
    }

    @Override
    @Transactional
    public List<GoodDTO> getSome(Integer pageNumber, Integer count) {

        List<Good> goodList = goodDao.findSome((pageNumber - 1) * 10, count);

        return GoodConverter.convert(goodList);

    }

    @Override
    @Transactional
    public Long getCountOfPage(Integer countOnPage) {

        Long countOfRaw = goodDao.getCountOfRow();

        Long countOfPage = countOfRaw / countOnPage;
        if (countOfRaw % countOnPage != 0) {
            countOfPage++;
        }
        return countOfPage;
    }

    private GoodDTO saveImage(GoodDTO goodDTO) throws IOException {
        String imageName = System.currentTimeMillis() + "";
        String fileLocation = environment.getProperty("upload.location") + environment.getProperty("goods.image.folder") + imageName + ".jpg";
        FileCopyUtils.copy(goodDTO.getFile().getBytes(), new File(fileLocation));
        goodDTO.setIconPath(imageName);
        return goodDTO;
    }

}
