package gmail.alexdudarkov.sportshop.service.model;


import java.math.BigDecimal;

public class OrderItemDTO {

    private Long Id;

    private GoodDTO good;

    private Integer count;

    private BigDecimal price;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public GoodDTO getGood() {
        return good;
    }

    public void setGood(GoodDTO good) {
        this.good = good;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "OrderItemDTO{" +
                "Id=" + Id +
                ", good=" + good +
                ", count=" + count +
                ", price=" + price +
                '}';
    }
}
