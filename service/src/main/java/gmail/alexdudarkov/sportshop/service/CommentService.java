package gmail.alexdudarkov.sportshop.service;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.model.CommentDTO;


public interface CommentService {
    void saveComment(CommentDTO commentDTO) throws DaoException;
}
