package gmail.alexdudarkov.sportshop.service.converters;

import gmail.alexdudarkov.sportshop.model.Basket;
import gmail.alexdudarkov.sportshop.model.BasketItem;
import gmail.alexdudarkov.sportshop.service.model.BasketItemDTO;

import java.util.ArrayList;
import java.util.List;

public class BasketItemConverter {

    public static BasketItemDTO convert(BasketItem basketItem){
        BasketItemDTO basketItemDTO =new BasketItemDTO();
        basketItemDTO.setId(basketItem.getId());
        basketItemDTO.setBasketId(basketItem.getBasket().getId());
        basketItemDTO.setCount(basketItem.getCount());
        basketItemDTO.setGood(GoodConverter.convertIntoDto(basketItem.getGood()));
        basketItemDTO.setPrice(basketItem.getPrice());
        return basketItemDTO;
    }

    public static BasketItem convert(BasketItemDTO basketItemDTO){
        BasketItem basketItem=new BasketItem();
        Basket basket=new Basket();
        basket.setId(basketItemDTO.getBasketId());
        basketItem.setId(basketItemDTO.getId());
        basketItem.setCount(basketItemDTO.getCount());
        basketItem.setGood(GoodConverter.convertIntoDao(basketItemDTO.getGood()));
        basketItem.setBasket(basket);
        basketItem.setPrice(basketItemDTO.getPrice());
       return basketItem;
    }

    public static List<BasketItemDTO> convert(List<BasketItem> basketItems) {
        List<BasketItemDTO> goodDTOList = new ArrayList<>();
        for (int i = 0; i < basketItems.size(); i++) {
            BasketItemDTO goodDto = BasketItemConverter.convert(basketItems.get(i));
            goodDTOList.add(goodDto);
        }
        return goodDTOList;
    }

}
