package gmail.alexdudarkov.sportshop.service.model;


import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;

public class GoodDTO {
    private Long id;
    private String model;
    private BigDecimal price;
    private String iconPath;
    private Boolean availability;
    private BrandGoodDTO brand;
    private TypeGoodDTO type;
    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public Boolean getAvailability() {
        return availability;
    }

    public void setAvailability(Boolean availability) {
        this.availability = availability;
    }

    public BrandGoodDTO getBrand() {
        return brand;
    }

    public void setBrand(BrandGoodDTO brand) {
        this.brand = brand;
    }

    public TypeGoodDTO getType() {
        return type;
    }

    public void setType(TypeGoodDTO type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "GoodDTO{" +
                "id=" + id +
                ", model='" + model + '\'' +
                ", price=" + price +
                ", iconPath='" + iconPath + '\'' +
                ", availability=" + availability +
                ", brand=" + brand +
                ", type=" + type +
                ", file=" + file +
                '}';
    }
}
