package gmail.alexdudarkov.sportshop.service;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.exception.ServiceException;
import gmail.alexdudarkov.sportshop.service.model.TypeGoodDTO;

import java.io.Serializable;
import java.util.List;

/**
 * Created by User-PC on 08.08.2017.
 */
public interface TypeGoodService {
    public List<TypeGoodDTO> findAll() throws ServiceException, DaoException;
    public Serializable save(TypeGoodDTO typeGoodDto) throws ServiceException, DaoException;
}
