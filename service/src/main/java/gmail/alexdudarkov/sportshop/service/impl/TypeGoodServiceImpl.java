package gmail.alexdudarkov.sportshop.service.impl;

import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.dao.TypeGoodDao;
import gmail.alexdudarkov.sportshop.dao.impl.TypeGoodDaoImpl;
import gmail.alexdudarkov.sportshop.model.TypeGood;
import gmail.alexdudarkov.sportshop.service.TypeGoodService;
import gmail.alexdudarkov.sportshop.service.converters.TypeGoodConverter;
import gmail.alexdudarkov.sportshop.service.model.TypeGoodDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Service
public class TypeGoodServiceImpl implements TypeGoodService {

    @Autowired
    private TypeGoodDao typeGoodDao;


    @Override
    @Transactional
    public Serializable save(TypeGoodDTO typeGoodDto) throws DaoException {
        convertDtoIntoDao(typeGoodDto);

        Long id = (Long) typeGoodDao.save(convertDtoIntoDao(typeGoodDto));

        return id;
    }


    static TypeGood convertDtoIntoDao(TypeGoodDTO typeGoodDto) {
        TypeGood typeGood = new TypeGood();
        typeGood.setId(typeGoodDto.getId());
        typeGood.setName(typeGoodDto.getName());
        return typeGood;
    }

    @Override
    @Transactional
    public List<TypeGoodDTO> findAll() throws DaoException {

        List<TypeGood> typeGoodList = typeGoodDao.findAll();
        List<TypeGoodDTO> typeGoodDTOList = new ArrayList<>();

        for (int i = 0; i < typeGoodList.size(); i++) {
            TypeGoodDTO brandGoodDto = TypeGoodConverter.convertIntoDto(typeGoodList.get(i));
            typeGoodDTOList.add(brandGoodDto);
        }
        return typeGoodDTOList;
    }
}
