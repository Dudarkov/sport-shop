package gmail.alexdudarkov.sportshop.service.impl;

import gmail.alexdudarkov.sportshop.dao.BrandGoodDao;
import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.dao.impl.BrandGoodDaoImpl;
import gmail.alexdudarkov.sportshop.model.BrandGood;
import gmail.alexdudarkov.sportshop.service.BrandGoodService;
import gmail.alexdudarkov.sportshop.service.converters.BrandGoodConverter;
import gmail.alexdudarkov.sportshop.service.model.BrandGoodDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Service
public class BrandGoodServiceImpl implements BrandGoodService {
    @Autowired
    private BrandGoodDao brandGoodDao;


    @Override
    @Transactional
    public Serializable save(BrandGoodDTO brandGoodDto) throws DaoException {

        Long id = (Long) brandGoodDao.save(convertDtoIntoDao(brandGoodDto));

        return id;
    }

    public static BrandGood convertDtoIntoDao(BrandGoodDTO brandGoodDto) {
        BrandGood brandGood = new BrandGood();
        brandGood.setId(brandGoodDto.getId());
        brandGood.setName(brandGoodDto.getName());
        return brandGood;
    }

    @Override
    @Transactional
    public List<BrandGoodDTO> findAll() throws DaoException {
        List<BrandGood> brandGoodList = brandGoodDao.findAll();
        List<BrandGoodDTO> brandGoodDTOList = new ArrayList<>();
        for (int i = 0; i < brandGoodList.size(); i++) {
            BrandGoodDTO brandGoodDto = BrandGoodConverter.convertIntoDto(brandGoodList.get(i));
            brandGoodDTOList.add(brandGoodDto);
        }
        return brandGoodDTOList;
    }
}
