package gmail.alexdudarkov.sportshop.service;


import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.model.Order;
import gmail.alexdudarkov.sportshop.model.Status;
import gmail.alexdudarkov.sportshop.service.model.OrderDTO;

import java.util.List;

public interface OrderService {
   void createOrderFromBasket() throws DaoException;
   List<OrderDTO> getUserOrders();
   List<OrderDTO> getAll() throws DaoException;
   OrderDTO getById(Long id) throws DaoException;
   void changeStatus(Long orderId, Status status) throws DaoException;
   Boolean checkAttachmentUser(Long orderId) throws DaoException;
}
