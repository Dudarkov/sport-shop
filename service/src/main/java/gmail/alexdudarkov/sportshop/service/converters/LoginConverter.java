package gmail.alexdudarkov.sportshop.service.converters;

import gmail.alexdudarkov.sportshop.model.User;
import gmail.alexdudarkov.sportshop.service.model.LoginUserDTO;


public class LoginConverter {

    public static User convertIntoDao(LoginUserDTO loginUserDTO){
        User user=new User();
        user.setUsername(loginUserDTO.getLogin());
        user.setPassword(loginUserDTO.getPassword());
        user.setRole(loginUserDTO.getRole());
        return user;
    }

    public static LoginUserDTO convertIntoDto(User user){
        LoginUserDTO loginUserDTO=new LoginUserDTO();
        loginUserDTO.setLogin(user.getUsername());
        loginUserDTO.setPassword(user.getPassword());
        loginUserDTO.setRole(user.getRole());
        return  loginUserDTO;
    }
}
