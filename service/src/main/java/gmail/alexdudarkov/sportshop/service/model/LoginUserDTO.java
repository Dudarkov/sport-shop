package gmail.alexdudarkov.sportshop.service.model;

import gmail.alexdudarkov.sportshop.model.Role;

import javax.validation.constraints.NotEmpty;


public class LoginUserDTO {
    @NotEmpty
    private String login;
    @NotEmpty
    private String password;

    private Role role;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "LoginUserDTO{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }
}
