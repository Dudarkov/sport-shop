package gmail.alexdudarkov.sportshop.service.impl;

import gmail.alexdudarkov.sportshop.dao.BasketDao;
import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.dao.OrderDao;
import gmail.alexdudarkov.sportshop.dao.UserDao;
import gmail.alexdudarkov.sportshop.model.Basket;
import gmail.alexdudarkov.sportshop.model.Order;
import gmail.alexdudarkov.sportshop.model.Status;
import gmail.alexdudarkov.sportshop.model.User;
import gmail.alexdudarkov.sportshop.service.OrderService;
import gmail.alexdudarkov.sportshop.service.UserService;
import gmail.alexdudarkov.sportshop.service.converters.OrderConverter;
import gmail.alexdudarkov.sportshop.service.model.OrderDTO;
import gmail.alexdudarkov.sportshop.service.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    private final BasketDao basketDao;
    private final OrderDao orderDao;
    private final UserDao userDao;

    @Autowired
    public OrderServiceImpl(BasketDao basketDao, OrderDao orderDao, UserDao userDao) {
        this.basketDao = basketDao;
        this.orderDao = orderDao;
        this.userDao = userDao;
    }

    @Override
    @Transactional
    public void createOrderFromBasket() throws DaoException {
        Long userId = UserUtil.getUserId();
        Basket basket = userDao.findById(userId).getBasket();
        User user=new User();
        user.setId(userId);
        Order order = OrderConverter.convert(basket);
        order.setStatus(Status.NEW);
        order.setUser(user);
        Date date=new Date();
        order.setDate(date);
        orderDao.save(order);


        Long basketId = basket.getId();
        basketDao.clearBasket(basketId);
    }

    @Transactional
    @Override
    public List<OrderDTO> getUserOrders() {
        Long userId = UserUtil.getUserId();
        List<Order> orders = orderDao.findByUserId(userId);
        List<OrderDTO> orderDTOS=OrderConverter.convert(orders);
        return orderDTOS;
    }

    @Transactional
    @Override
    public List<OrderDTO> getAll() throws DaoException {
        List<Order> orders = orderDao.findAll();
        List<OrderDTO> orderDTOS=OrderConverter.convert(orders);
        return orderDTOS;
    }

    @Transactional
    @Override
    public OrderDTO getById(Long id) throws DaoException {
       Order order= orderDao.findById(id);
       return OrderConverter.convert(order);
    }

    @Transactional
    @Override
    public void changeStatus(Long orderId, Status status) throws DaoException {
        Order order=orderDao.findById(orderId);
        order.setStatus(status);
        orderDao.saveOrUpdate(order);
    }

    @Override
    @Transactional
    public Boolean checkAttachmentUser(Long orderId) throws DaoException {
       Long userId =UserUtil.getUserId();
       Order order=orderDao.findById(orderId);
       if(order!=null && order.getUser().getId().equals(userId)){
           return true;
       }
       else return false;
    }
}
