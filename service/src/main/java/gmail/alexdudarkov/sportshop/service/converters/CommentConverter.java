package gmail.alexdudarkov.sportshop.service.converters;

import gmail.alexdudarkov.sportshop.model.Comment;
import gmail.alexdudarkov.sportshop.model.User;
import gmail.alexdudarkov.sportshop.service.model.CommentDTO;


public class CommentConverter {

    public static CommentDTO convert(Comment comment){
        CommentDTO commentDTO=new CommentDTO();
        commentDTO.setId(comment.getId());
        commentDTO.setContent(comment.getContent());
        commentDTO.setEmail(comment.getEmail());
        commentDTO.setUserId(comment.getUser().getId());
        return commentDTO;
    }

    public static Comment convert(CommentDTO commentDTO){
        Comment comment=new Comment();
        comment.setContent(commentDTO.getContent());
        comment.setEmail(commentDTO.getEmail());
        comment.setId(commentDTO.getId());
        User user=new User();
        user.setId(commentDTO.getUserId());
        comment.setUser(user);
        return comment;
    }
}
