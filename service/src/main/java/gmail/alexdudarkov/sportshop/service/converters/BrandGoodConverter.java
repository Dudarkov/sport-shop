package gmail.alexdudarkov.sportshop.service.converters;


import gmail.alexdudarkov.sportshop.model.BrandGood;
import gmail.alexdudarkov.sportshop.service.model.BrandGoodDTO;


public class BrandGoodConverter {

    public static BrandGoodDTO convertIntoDto(BrandGood brandGood) {
        BrandGoodDTO brandGoodDTO = new BrandGoodDTO();
        brandGoodDTO.setId(brandGood.getId());
        brandGoodDTO.setName(brandGood.getName());
        return brandGoodDTO;
    }

    public static BrandGood convertIntoDao(BrandGoodDTO brandGoodDTO) {
        BrandGood brandGood = new BrandGood();
        brandGood.setId(brandGoodDTO.getId());
        brandGood.setName(brandGoodDTO.getName());
        return brandGood;
    }
}
