package gmail.alexdudarkov.sportshop.service.model;


import java.math.BigDecimal;

public class BasketItemDTO {

    private Long id;
    private Long basketId;
    private GoodDTO good;
    private Integer count;
    private BigDecimal price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GoodDTO getGood() {
        return good;
    }

    public void setGood(GoodDTO good) {
        this.good = good;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getBasketId() {
        return basketId;
    }

    public void setBasketId(Long basketId) {
        this.basketId = basketId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BasketItemDTO)) return false;

        BasketItemDTO that = (BasketItemDTO) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (good != null ? !good.equals(that.good) : that.good != null) return false;
        if (count != null ? !count.equals(that.count) : that.count != null) return false;
        return price != null ? price.equals(that.price) : that.price == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (good != null ? good.hashCode() : 0);
        result = 31 * result + (count != null ? count.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BasketItemDTO{" +
                "id=" + id +
                ", basketId=" + basketId +
                ", good=" + good +
                ", count=" + count +
                ", price=" + price +
                '}';
    }
}
