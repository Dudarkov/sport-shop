package gmail.alexdudarkov.sportshop.service;


import gmail.alexdudarkov.sportshop.dao.DaoException;
import gmail.alexdudarkov.sportshop.service.model.BasketItemDTO;

import java.io.Serializable;

public interface BasketItemService {
    Serializable save(BasketItemDTO basketItemDto) throws DaoException;
}
