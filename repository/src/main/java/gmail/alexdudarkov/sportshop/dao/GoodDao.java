package gmail.alexdudarkov.sportshop.dao;

import gmail.alexdudarkov.sportshop.model.Good;


public interface GoodDao extends GenericDao<Good> {

}
