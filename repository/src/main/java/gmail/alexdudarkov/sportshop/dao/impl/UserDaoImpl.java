package gmail.alexdudarkov.sportshop.dao.impl;

import gmail.alexdudarkov.sportshop.dao.UserDao;
import gmail.alexdudarkov.sportshop.model.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl extends AbstractGenericDao<User> implements UserDao {
    @Override
    public User findByLogin(String login) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("username", login));
        Object uniqueResult = criteria.uniqueResult();
        return uniqueResult != null ? (User) uniqueResult : null;
    }
}
