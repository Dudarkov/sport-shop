package gmail.alexdudarkov.sportshop.dao;


import gmail.alexdudarkov.sportshop.model.Comment;

public interface CommentDao extends GenericDao<Comment> {
}
