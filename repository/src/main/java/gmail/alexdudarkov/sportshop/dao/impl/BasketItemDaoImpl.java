package gmail.alexdudarkov.sportshop.dao.impl;

import gmail.alexdudarkov.sportshop.dao.BasketItemDao;
import gmail.alexdudarkov.sportshop.model.BasketItem;
import org.springframework.stereotype.Repository;

@Repository
public class BasketItemDaoImpl extends AbstractGenericDao<BasketItem> implements BasketItemDao {
}
