package gmail.alexdudarkov.sportshop.dao.impl;


import gmail.alexdudarkov.sportshop.dao.CommentDao;
import gmail.alexdudarkov.sportshop.model.Comment;
import org.springframework.stereotype.Repository;

@Repository
public class CommentDaoImpl extends AbstractGenericDao<Comment> implements CommentDao {

}
