package gmail.alexdudarkov.sportshop.dao.impl;


import gmail.alexdudarkov.sportshop.dao.GenericDao;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;


@Repository
public abstract class AbstractGenericDao<E> implements GenericDao<E> {


    private final Class<E> entityClass;
    @Autowired
    private SessionFactory sessionFactory;


    public AbstractGenericDao() {
        this.entityClass = (Class<E>) ((ParameterizedType) this.getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];

    }

    protected Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public Serializable save(E entity) {
        return getSession().save(entity);
    }

    @Override
    public void saveOrUpdate(E entity) {
        getSession().saveOrUpdate(entity);
    }

    @Override
    public void delete(E entity) {
        getSession().delete(entity);
    }


    @Override
    public List<E> findAll() {

        return getSession().createCriteria(this.entityClass).list();
    }

    @Override
    public List<E> findAllByExample(E entity) {
        Example example = Example.create(entity).ignoreCase().enableLike().excludeZeroes();
        return getSession().createCriteria(this.entityClass).add(example).list();
    }

    @Override
    public E findById(Serializable id) {
        return (E) getSession().get(this.entityClass, id);
    }

    @Override
    public Long getCountOfRow() {
        return  (Long)getSession().createCriteria(this.entityClass)
                .setProjection(Projections.rowCount()).uniqueResult();
    }

    @Override
    public List<E> findSome(int firstResult, int maxResult) {
        Criteria cr = getSession().createCriteria(this.entityClass);
        cr.setFirstResult(firstResult);
        cr.setMaxResults(maxResult);
        return cr.list();
    }

}
