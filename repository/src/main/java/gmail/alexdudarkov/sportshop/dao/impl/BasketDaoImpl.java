package gmail.alexdudarkov.sportshop.dao.impl;

import gmail.alexdudarkov.sportshop.dao.BasketDao;
import gmail.alexdudarkov.sportshop.model.Basket;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;


@Repository
public class BasketDaoImpl extends AbstractGenericDao<Basket> implements BasketDao {


    public void clearBasket(Long basketId) {
        String hql = "delete from BasketItem where basket.id= :basketId";
        try {

            //   where BasketItem.basket.id= :basketId
            Query query = getSession().createQuery(hql);
            query.setParameter("basketId", basketId);
            query.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
