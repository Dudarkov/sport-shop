package gmail.alexdudarkov.sportshop.dao.impl;


import gmail.alexdudarkov.sportshop.dao.OrderDao;
import gmail.alexdudarkov.sportshop.model.Order;
import org.springframework.stereotype.Repository;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class OrderDaoImpl extends AbstractGenericDao<Order> implements OrderDao {
    @Override
    public List<Order> findByUserId(Long idUser) {
        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<Order> criteria = builder.createQuery(Order.class);
        Root<Order> root = criteria.from(Order.class);
        criteria.select(root);
        criteria.where(builder.equal(root.get("user").get("id"), idUser));
        List<Order> orders=getSession().createQuery(criteria).getResultList();
        return orders;

    }
}
