package gmail.alexdudarkov.sportshop.dao;

import gmail.alexdudarkov.sportshop.model.BrandGood;


public interface BrandGoodDao extends GenericDao<BrandGood> {
}
