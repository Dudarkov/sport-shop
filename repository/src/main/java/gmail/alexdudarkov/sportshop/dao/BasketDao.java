package gmail.alexdudarkov.sportshop.dao;

import gmail.alexdudarkov.sportshop.model.Basket;


public interface BasketDao extends GenericDao<Basket>{
    void clearBasket(Long basketId);
}
