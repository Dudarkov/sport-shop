package gmail.alexdudarkov.sportshop.dao.impl;


import gmail.alexdudarkov.sportshop.dao.TypeGoodDao;
import gmail.alexdudarkov.sportshop.model.TypeGood;
import org.springframework.stereotype.Repository;

@Repository
public class TypeGoodDaoImpl extends AbstractGenericDao<TypeGood> implements TypeGoodDao {

}
