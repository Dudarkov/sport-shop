package gmail.alexdudarkov.sportshop.dao;

import gmail.alexdudarkov.sportshop.model.OrderItem;

import java.util.List;


public interface OrderItemDao extends GenericDao<OrderItem> {
    List<OrderItem> findByOrderId(Long idOrder);
}
