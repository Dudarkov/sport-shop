package gmail.alexdudarkov.sportshop.dao.impl;

import gmail.alexdudarkov.sportshop.dao.OrderItemDao;
import gmail.alexdudarkov.sportshop.model.OrderItem;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class OrderItemDaoImpl extends AbstractGenericDao<OrderItem> implements OrderItemDao {

    @Override
    public List<OrderItem> findByOrderId(Long idOrder) {
        CriteriaBuilder builder = getSession().getCriteriaBuilder();
        CriteriaQuery<OrderItem> criteria = builder.createQuery(OrderItem.class);
        Root<OrderItem> root = criteria.from(OrderItem.class);
        criteria.select(root);
        criteria.where(builder.equal(root.get("order").get("id"), idOrder));
        List<OrderItem> orderItems=getSession().createQuery(criteria).getResultList();
        return orderItems;

    }
}
