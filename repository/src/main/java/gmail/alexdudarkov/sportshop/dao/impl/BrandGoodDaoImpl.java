package gmail.alexdudarkov.sportshop.dao.impl;

import gmail.alexdudarkov.sportshop.dao.BrandGoodDao;
import gmail.alexdudarkov.sportshop.model.BrandGood;
import org.springframework.stereotype.Repository;

@Repository
public class BrandGoodDaoImpl extends AbstractGenericDao<BrandGood> implements BrandGoodDao {

}
