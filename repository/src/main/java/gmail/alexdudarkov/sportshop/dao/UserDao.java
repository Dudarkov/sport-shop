package gmail.alexdudarkov.sportshop.dao;

import gmail.alexdudarkov.sportshop.model.User;


public interface UserDao extends GenericDao<User>{

    User findByLogin(String login);
}
