package gmail.alexdudarkov.sportshop.dao;

import gmail.alexdudarkov.sportshop.model.Order;

import java.util.List;


public interface OrderDao extends GenericDao<Order> {
    List<Order> findByUserId(Long idUser);
}
