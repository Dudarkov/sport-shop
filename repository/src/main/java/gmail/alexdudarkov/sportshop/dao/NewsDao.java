package gmail.alexdudarkov.sportshop.dao;

import gmail.alexdudarkov.sportshop.model.News;


public interface NewsDao extends GenericDao<News> {
}
