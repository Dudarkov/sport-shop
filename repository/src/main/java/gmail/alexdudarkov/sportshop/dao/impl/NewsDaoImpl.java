package gmail.alexdudarkov.sportshop.dao.impl;

import gmail.alexdudarkov.sportshop.dao.NewsDao;
import gmail.alexdudarkov.sportshop.model.News;
import org.springframework.stereotype.Repository;

@Repository
public class NewsDaoImpl extends AbstractGenericDao<News> implements NewsDao {

}
