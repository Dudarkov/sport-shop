package gmail.alexdudarkov.sportshop.dao;

import gmail.alexdudarkov.sportshop.model.BasketItem;


public interface BasketItemDao extends GenericDao<BasketItem> {
}
