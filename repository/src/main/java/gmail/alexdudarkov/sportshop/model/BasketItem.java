package gmail.alexdudarkov.sportshop.model;


import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
@Entity
@Table(name = "basket_item")
public class BasketItem implements Serializable{
    private static final long serialVersionUID = 3712782542598628784L;
    @Id

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "basket_item_id")
    private Long id;
    @ManyToOne(optional = false)
    @JoinColumn(name="good_id", nullable = false)
    private Good good;
    @Column(name = "count")
    private Integer count;
    @Column (name = "price", precision = 8, scale = 2)
    private BigDecimal price;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "basket_id")
    private Basket basket;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Good getGood() {
        return good;
    }

    public void setGood(Good good) {
        this.good = good;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Basket getBasket() {
        return basket;
    }

    public void setBasket(Basket basket) {
        this.basket = basket;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BasketItem)) return false;

        BasketItem that = (BasketItem) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (good != null ? !good.equals(that.good) : that.good != null) return false;
        if (count != null ? !count.equals(that.count) : that.count != null) return false;
        return price != null ? price.equals(that.price) : that.price == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (good != null ? good.hashCode() : 0);
        result = 31 * result + (count != null ? count.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        return result;
    }
}
