package gmail.alexdudarkov.sportshop.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by User-PC on 03.08.2017.
 */
@Entity
@Table(name="images_name")
public class ImagesGood implements Serializable{
    private static final long serialVersionUID = -2159053534008798253L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "images_good_id")
    private Long id;
    @Column(name = "name")
    private String name;
    @ManyToOne(optional = false)
    @JoinColumn(name = "good_id", nullable = false)
    private Good good;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Good getGood() {
        return good;
    }

    public void setGood(Good good) {
        this.good = good;
    }

    @Override
    public String toString() {
        return "ImagesGood{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
