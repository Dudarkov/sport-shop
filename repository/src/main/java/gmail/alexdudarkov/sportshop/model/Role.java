package gmail.alexdudarkov.sportshop.model;


public enum Role {
    ROLE_USER,

    ROLE_ADMIN,
    ROLE_SUPER_ADMIN
}
