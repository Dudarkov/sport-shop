package gmail.alexdudarkov.sportshop.model;

public enum  UserStatus {

    ENABLE,
    DISABLE
}
